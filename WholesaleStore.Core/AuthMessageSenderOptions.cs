﻿namespace WholesaleStore.Core
{
    public class AuthMessageSenderOptions
    {
        public string SendGridKey { get; set; }
    }
}
