﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.Services
{
    public class ProductReviewService : IProductReviewService
    {
        public Task<bool> Add(ProductReviewModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(ProductReviewModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<ProductReviewModel>> Get(ProductReviewFilter Filters)
        {
            throw new NotImplementedException();
        }

        public Task<List<ProductReviewModel>> Get(ProductReviewFilter Filters, string includedProps)
        {
            throw new NotImplementedException();
        }

        public Task<ProductReviewModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(ProductReviewModel model)
        {
            throw new NotImplementedException();
        }
    }
}
