﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Add(CategoryModel model)
        {
            if (model == null) return false;

            model.CreatedOnUtc = DateTime.UtcNow;
            model.UpdatedOnUtc = DateTime.UtcNow;
            model.DisplayOrder = 1;

            await _unitOfWork.CategoryRepository.AddAsync(_mapper.Map<Category>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<bool> Delete(CategoryModel model)
        {
            if (model == null) return false;

            return await Delete(model.Id);
        }

        public async Task<bool> Delete(int id)
        {
            if(id <= 0) return false;

            await _unitOfWork.CategoryRepository.DeleteAsync(id);
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<List<CategoryModel>> Get(CategoryFilter Filters)
        {
            return await Get(Filters, "");
        }

        public async Task<List<CategoryModel>> Get(CategoryFilter Filters, string includedProps)
        {
            Expression<Func<Category, bool>> _Expression = null;

            if (Filters != null)
            {
                _Expression =
                (
                    x => (!string.IsNullOrEmpty(Filters.Name) ? x.Name.ToLower().Contains(Filters.Name.ToLower()) : true)
                      && (Filters.ParentCategoryId >= 0 ? x.ParentCategoryId == Filters.ParentCategoryId : true)
                );
            }

            List<Category> categories = await _unitOfWork.CategoryRepository.GetAsync(_Expression, o => o.OrderBy(c => c.CreatedOnUtc), includedProps) as List<Category>;

            return _mapper.Map<List<CategoryModel>>(categories);
        }

        public async Task<List<CategoryModel>> GetAllCategoriesWithSubCategories()
        {
            CategoryFilter filter = new CategoryFilter();
            filter.ParentCategoryId = 0;
            List<CategoryModel> categories = await Get(filter, "");

            await GetSubCategories(categories, filter);

            return categories;
        }

        private async Task GetSubCategories(List<CategoryModel> categories, CategoryFilter filter)
        {
            foreach (var category in categories)
            {
                filter.ParentCategoryId = category.Id;

                if (category.ChildCategories == null) category.ChildCategories = new List<CategoryModel>();

                category.ChildCategories = await Get(filter, "");

                if (category.ChildCategories != null && category.ChildCategories.Count > 0) await GetSubCategories(category.ChildCategories, filter);
            }
        }

        public async Task<CategoryModel> GetByID(int id)
        {
            if(id <= 0) return null;

            Category category = await _unitOfWork.CategoryRepository.GetByIDAsync(id);

            return _mapper.Map<CategoryModel>(category);
        }

        public async Task<bool> Update(CategoryModel model)
        {
            if(model == null) return false;

            await _unitOfWork.CategoryRepository.UpdateAsync(_mapper.Map<Category>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<bool> Activate(int categoryId)
        {
            if (categoryId <= 0) return false;

            Category category = await _unitOfWork.CategoryRepository.GetByIDAsync(categoryId);
            category.Deleted = !category.Deleted;

            await _unitOfWork.CategoryRepository.UpdateAsync(category);
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<List<CategoryModel>> GetAllCategories()
        {
            List<Category> categories = await _unitOfWork.CategoryRepository.GetAllAsync() as List<Category>;
            List<CategoryModel> categoriesModel = _mapper.Map<List<CategoryModel>>(categories);
            await GetParentCategory(categoriesModel);
            return categoriesModel;
        }

        private async Task GetParentCategory(List<CategoryModel> categories)
        {
            foreach(CategoryModel category in categories)
            {
                category.ParentCategory = _mapper.Map<CategoryModel>(await _unitOfWork.CategoryRepository.GetByIDAsync(category.ParentCategoryId));
            }
        }
    }
}
