﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IUserRoleService _userRoleService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<UserService> _logger;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, IUserRoleService userRoleService,
           UserManager<ApplicationUser> userManager, ILogger<UserService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userRoleService = userRoleService;
            _userManager = userManager;
            _logger = logger;
        }

        public Task<bool> Add(UserModel model)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> ChangePassword(string phoneNumber, string oldPassword, string newPassword)
        {
            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.PhoneNumber == phoneNumber);

            if (user == null) return false;

            var changePasswordResult = await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);

            return changePasswordResult.Succeeded;
        }

        public async Task<bool> ConfirmPhoneNumber(string phoneNumber)
        {
            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.PhoneNumber == phoneNumber);

            if(user == null) return false;

            user.PhoneNumberConfirmed = true;

            await _unitOfWork.UserRepository.UpdateAsync(user);
            await _unitOfWork.UserRepository.SaveAsync();

            return true;
        }

        public Task<bool> Delete(UserModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<UserModel>> Get(UserFilter Filters)
        {
            return await Get(Filters, "");
        }

        public async Task<List<UserModel>> Get(UserFilter Filters, string includedProps)
        {
            Expression<Func<ApplicationUser, bool>> _Expression = null;

            if (Filters != null)
            {
                _Expression =
                (
                    x => (!string.IsNullOrEmpty(Filters.Name) ? (x.SystemName.ToLower().Contains(Filters.Name.ToLower()) || x.UserName.ToLower().Contains(Filters.Name.ToLower())) : true)
                );
            }

            List<ApplicationUser> users = await _unitOfWork.UserRepository.GetAsync(_Expression, o => o.OrderBy(u => u.UserName), includedProps, Filters.MaxRows, (Filters.CurrentPage - 1) * Filters.MaxRows) as List<ApplicationUser>;
            int count = await _unitOfWork.UserRepository.GetCountAsync(_Expression);

            double pageCount = (double)((decimal)count / Convert.ToDecimal(Filters.MaxRows));
            Filters.PageCount = (int)Math.Ceiling(pageCount);
            Filters.CurrentPage = Filters.CurrentPage;

            return _mapper.Map<List<UserModel>>(users);
        }

        public Task<UserModel> GetAdminUserByStoreId(int storeId)
        {
            throw new NotImplementedException();
        }

        public async Task<UserModel> GetByEmail(string email)
        {
            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.Email == email);
            UserModel userModel = _mapper.Map<UserModel>(user);

            return userModel;
        }

        public async Task<UserModel> GetByGuid(Guid userGuid)
        {
            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.UserGuid == userGuid);
            UserModel userModel = _mapper.Map<UserModel>(user);

            return userModel;
        }

        public async Task<UserModel> GetByID(int id)
        {
            if (id <= 0) return null;
            ApplicationUser user = await _unitOfWork.UserRepository.GetByIDAsync(id);
            return _mapper.Map<UserModel>(user);
        }

        public async Task<UserModel> GetByName(string name)
        {
            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.UserName == name);

            UserModel userModel = _mapper.Map<UserModel>(user);

            if (user.UserRoles == null || user.UserRoles.Count <= 0)
            {
                var userRoleFilter = new UserRoleFilter() { UserId = user.Id };
                userModel.Roles = (await _userRoleService.Get(userRoleFilter)).Select(ur => ur.Role).ToList();
            }

            return userModel;
        }

        public async Task<UserModel> GetByPhoneNumber(string phoneNumber)
        {
            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.PhoneNumber == phoneNumber);
            UserModel userModel = _mapper.Map<UserModel>(user);

            return userModel;
        }

        public async Task<UserModel> Register(RegisterModel registerModel)
        {
            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.PhoneNumber == registerModel.PhoneNumber);

            if (user != null) return null;

            user = new ApplicationUser
            {
                UserName = registerModel.Email,
                Email = registerModel.Email,
                PhoneNumber = registerModel.PhoneNumber,
                ComapnyName = registerModel.CompanyName,
                Address = new Address() { Address1 = registerModel.Address, CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                EmailConfirmed = true, // Remove it from here when mobile works
                CreatedOnUtc = DateTime.UtcNow,
                LastActivityDateUtc = DateTime.UtcNow,
                LastLoginDateUtc = DateTime.UtcNow,
                UserGuid = Guid.NewGuid(),
                SystemName = registerModel.Username
            };

            user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, registerModel.Password);

            await _unitOfWork.UserRepository.AddAsync(user);
            await _unitOfWork.SaveAsync();

            return _mapper.Map<UserModel>(user);
        }

        public async Task<UserModel> Register(string username, string email, string phoneNumber, string password)
        {
            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.Email == email);

            if (user != null) return null;

            user = new ApplicationUser
            {
                UserName = email,
                Email = email,
                PhoneNumber = phoneNumber,
                //ComapnyName = registerModel.CompanyName,
                //Address = new Address() { Address1 = registerModel.Address, CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                CreatedOnUtc = DateTime.UtcNow,
                LastActivityDateUtc = DateTime.UtcNow,
                LastLoginDateUtc = DateTime.UtcNow,
                UserGuid = Guid.NewGuid(),
                SystemName = username
            };

            user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, password);

            await _unitOfWork.UserRepository.AddAsync(user);
            await _unitOfWork.SaveAsync();

            return _mapper.Map<UserModel>(user);
        }

        public async Task<bool> ResetPassword(string phoneNumber, string password)
        {
            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.PhoneNumber == phoneNumber);

            if (user == null) return false;

            var result = await _userManager.ResetPasswordAsync(user, await _userManager.GeneratePasswordResetTokenAsync(user), password);

            return result.Succeeded;
        }

        public Task<bool> Update(UserModel model)
        {
            throw new NotImplementedException();
        }
    }
}
