﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.Services
{
    public class ShoppingCartItemService : IShoppingCartItemService
    {
        public Task<bool> Add(ShoppingCartItemModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(ShoppingCartItemModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<ShoppingCartItemModel>> Get(ShoppingCartItemFilter Filters)
        {
            throw new NotImplementedException();
        }

        public Task<List<ShoppingCartItemModel>> Get(ShoppingCartItemFilter Filters, string includedProps)
        {
            throw new NotImplementedException();
        }

        public Task<ShoppingCartItemModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(ShoppingCartItemModel model)
        {
            throw new NotImplementedException();
        }
    }
}
