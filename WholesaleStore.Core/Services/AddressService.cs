﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class AddressService : IAddressService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public AddressService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Add(AddressModel model)
        {
            if(model == null) return false;

            await _unitOfWork.AddressRepository.AddAsync(_mapper.Map<Address>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<bool> Delete(AddressModel model)
        {
            if (model == null) return false ;

            return await Delete(model.Id);
        }

        public async Task<bool> Delete(int id)
        {
            if (id == 0) return false;

            Address address = await _unitOfWork.AddressRepository.GetByIDAsync(id);
            if (address == null) return false;

            await _unitOfWork.AddressRepository.DeleteAsync(address);
            await _unitOfWork.SaveAsync();

             return true;
        }

        public async Task<List<AddressModel>> Get(AddressFilter Filters)
        {
            return await Get(Filters, string.Empty);
        }

        public async Task<List<AddressModel>> Get(AddressFilter Filters, string includedProps)
        {
            List<Address> addresses = await _unitOfWork.AddressRepository.GetAsync(null, o => o.OrderBy(a => a.CreatedDate), includedProps) as List<Address>;
            return _mapper.Map<List<AddressModel>>(addresses);
        }

        public async Task<AddressModel> GetByID(int id)
        {
            if(id <= 0) return null;

            Address address = await _unitOfWork.AddressRepository.GetByIDAsync(id);

            return _mapper.Map<AddressModel>(address);
        }

        public async Task<bool> Update(AddressModel model)
        {
            if (model == null) return false;

            await _unitOfWork.AddressRepository.UpdateAsync(_mapper.Map<Address>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }
    }
}
