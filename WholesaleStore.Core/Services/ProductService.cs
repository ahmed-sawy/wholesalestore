﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public static IWebHostEnvironment _environment;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper, IWebHostEnvironment environment)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _environment = environment;
        }

        public async Task<bool> Add(ProductModel model, List<IFormFile> pictures = null, IFormFile mainPicture = null)
        {
            if (model == null) return false;

            Product product = _mapper.Map<Product>(model);

            product.CreatedOnUtc = DateTime.UtcNow;
            product.UpdatedOnUtc = DateTime.UtcNow;
            product.Store = await _unitOfWork.StoreRepository.GetByIDAsync(product.StoreId);

            List<ProductPicture> pp = new List<ProductPicture>();

            if (pictures != null && pictures.Count > 0)
            {
                foreach (var picture in pictures)
                {
                    Picture _picture = new Picture();
                    _picture.Name = await UploadImage(picture);

                    ProductPicture productPicture = new ProductPicture();
                    productPicture.Picture = _picture;
                    productPicture.Product = product;
                    productPicture.DisplayOrder = 1;

                    pp.Add(productPicture);

                    await _unitOfWork.PictureRepository.AddAsync(_picture);
                    await _unitOfWork.ProductPictureRepository.AddAsync(productPicture);
                }
            }

            if (mainPicture != null)
            {
                product.MainPicture = await UploadImage(mainPicture);
            }

            product.ProductPictures = pp;

            await _unitOfWork.ProductRepository.AddAsync(product);
            await _unitOfWork.SaveAsync();

            return true;
        }

        public Task<bool> Add(ProductModel model)
        {

            throw new NotImplementedException();
        }

        private void DeleteCurrentImageIfExists(string image)
        {
            if (!string.IsNullOrEmpty(image))
            {
                string imagePath = Path.Combine(_environment.WebRootPath, "Upload", "ProductsImages", image);

                if (System.IO.File.Exists(imagePath))
                {
                    System.IO.File.Delete(imagePath);
                }
            }

        }

        private async Task<List<string>> UploadImages(List<IFormFile> images)
        {
            List<string> result = new List<string>();
            foreach (IFormFile file in images)
            {
                result.Add(await UploadImage(file));
            }
            return result;
        }

        private async Task<string> UploadImage(IFormFile image)
        {
            if (!Directory.Exists(Path.Combine(_environment.WebRootPath, "Upload", "ProductsImages")))
            {
                Directory.CreateDirectory(Path.Combine(_environment.WebRootPath, "Upload", "ProductsImages"));
            }

            string fileName = DateTime.Now.ToString("MM-dd-yyyy_hmmsstt") + "_" + image.FileName;

            using (FileStream fileStream = System.IO.File.Create(Path.Combine(_environment.WebRootPath, "Upload", "ProductsImages", fileName)))
            {
                await image.CopyToAsync(fileStream);
                fileStream.Flush();
            }
            return fileName;
        }

        public Task<bool> Delete(ProductModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ProductModel>> Get(ProductFilter Filters)
        {
            return await Get(Filters, "");
        }

        public async Task<List<ProductModel>> Get(ProductFilter Filters, string includedProps)
        {
            Expression<Func<Product, bool>> _Expression = null;

            if (Filters != null)
            {
                _Expression =
                (
                    x => (Filters.StoreId > -1 ? x.StoreId == Filters.StoreId : true)
                      && (!string.IsNullOrEmpty(Filters.Name) ? x.Name.ToLower().Contains(Filters.Name.ToLower()) : true)
                      && (Filters.StockQuantity > 0 ? x.StockQuantity == Filters.StockQuantity : true)
                      && (Filters.OrderMaximumQuantity > 0 ? x.OrderMaximumQuantity == Filters.OrderMaximumQuantity : true)
                      && (Filters.OrderMinimumQuantity > 0 ? x.OrderMinimumQuantity == Filters.OrderMinimumQuantity : true)
                      && (Filters.Price > 0 ? x.Price == Filters.Price : true)
                      && (Filters.ProductCost > 0 ? x.ProductCost == Filters.ProductCost : true)
                );
            }

            List<Product> products = await _unitOfWork.ProductRepository.GetAsync(_Expression, o => o.OrderBy(s => s.DisplayOrder), includedProps, Filters.MaxRows, (Filters.CurrentPage - 1) * Filters.MaxRows) as List<Product>;

            int count = await _unitOfWork.ProductRepository.GetCountAsync(_Expression);

            double pageCount = (double)((decimal)count / Convert.ToDecimal(Filters.MaxRows));
            Filters.PageCount = (int)Math.Ceiling(pageCount);
            Filters.CurrentPage = Filters.CurrentPage;

            return _mapper.Map<List<ProductModel>>(products);
        }

        public Task<ProductModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(ProductModel model)
        {
            throw new NotImplementedException();
        }
    }
}
