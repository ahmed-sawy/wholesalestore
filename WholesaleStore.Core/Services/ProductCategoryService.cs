﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.Services
{
    public class ProductCategoryService : IProductCategoryService
    {
        public Task<bool> Add(ProductCategoryModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(ProductCategoryModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<ProductCategoryModel>> Get(ProductCategoryFilter Filters)
        {
            throw new NotImplementedException();
        }

        public Task<List<ProductCategoryModel>> Get(ProductCategoryFilter Filters, string includedProps)
        {
            throw new NotImplementedException();
        }

        public Task<ProductCategoryModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(ProductCategoryModel model)
        {
            throw new NotImplementedException();
        }
    }
}
