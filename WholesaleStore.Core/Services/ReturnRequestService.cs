﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.Services
{
    public class ReturnRequestService : IReturnRequestService
    {
        public Task<bool> Add(ReturnRequestModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(ReturnRequestModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<ReturnRequestModel>> Get(ReturnRequestFilter Filters)
        {
            throw new NotImplementedException();
        }

        public Task<List<ReturnRequestModel>> Get(ReturnRequestFilter Filters, string includedProps)
        {
            throw new NotImplementedException();
        }

        public Task<ReturnRequestModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(ReturnRequestModel model)
        {
            throw new NotImplementedException();
        }
    }
}
