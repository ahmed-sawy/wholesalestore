﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class AuthenticateService : IAuthenticateService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AuthenticateService(SignInManager<ApplicationUser> signInManager, IUnitOfWork unitOfWork)
        {
            _signInManager = signInManager;
            _unitOfWork = unitOfWork;
        }

        public async Task<SignInResult> Authenticate(LoginModel loginModel)
        {
            loginModel.PhoneNumber = loginModel.PhoneNumber.Trim();
            loginModel.Password = loginModel.Password.Trim();

            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.PhoneNumber == loginModel.PhoneNumber);
            if(user == null || !user.PhoneNumberConfirmed) return null;
            return await _signInManager.CheckPasswordSignInAsync(user, loginModel.Password, lockoutOnFailure: false);
        }

        public async Task<SignInResult> Authenticate(string email, string password)
        {
            email = email.Trim();
            password = password.Trim();

            ApplicationUser user = await _unitOfWork.UserRepository.GetOneAsync(u => u.Email == email);
            if (user == null || !user.EmailConfirmed) return null;
            return await _signInManager.CheckPasswordSignInAsync(user, password, lockoutOnFailure: false);
        }

        public IEnumerable<Claim> GenerateClaims(UserModel userModel)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, userModel.SystemName ),
                            new Claim("USERID", userModel.Id.ToString()),
                            new Claim("EMAIL", userModel.Email ?? ""),
                            new Claim(ClaimTypes.MobilePhone, userModel.PhoneNumber)
                            //new Claim(ClaimTypes.Role, adminDTO.AdminRole)
            };

            if(userModel.Roles != null)
            {
                foreach (var role in userModel.Roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, role.ToString()));
                }
            }

            return claims;
        }
    }
}
