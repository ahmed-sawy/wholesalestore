﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.Services
{
    public class UserAddressService : IUserAddressService
    {
        public Task<bool> Add(UserAddressModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(UserAddressModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<UserAddressModel>> Get(UserAddressFilter Filters)
        {
            throw new NotImplementedException();
        }

        public Task<List<UserAddressModel>> Get(UserAddressFilter Filters, string includedProps)
        {
            throw new NotImplementedException();
        }

        public Task<UserAddressModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(UserAddressModel model)
        {
            throw new NotImplementedException();
        }
    }
}
