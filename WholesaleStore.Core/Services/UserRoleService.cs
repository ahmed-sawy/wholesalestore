﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class UserRoleService : IUserRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserRoleService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Task<bool> Add(UserRoleModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(UserRoleModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<UserRoleModel>> Get(UserRoleFilter Filters)
        {
            Expression<Func<ApplicationUserRole, bool>> _Expression = null;

            if (Filters != null)
            {
                _Expression =
                (
                    x => (Filters.UserId > 0 ? x.UserId == Filters.UserId : true)
                      && (Filters.RoleId > 0 ? x.RoleId == Filters.RoleId : true)
                );
            }

            List<ApplicationUserRole> userRoles = await _unitOfWork.UserRoleRepository.GetAsync(_Expression, null, "Role") as List<ApplicationUserRole>;

            return _mapper.Map<List<UserRoleModel>>(userRoles);
        }

        public Task<List<UserRoleModel>> Get(UserRoleFilter Filters, string includedProps)
        {
            throw new NotImplementedException();
        }

        public Task<UserRoleModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(UserRoleModel model)
        {
            throw new NotImplementedException();
        }
    }
}
