﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class StoreService : IStoreService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public StoreService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> ActivateStore(int storeId)
        {
            if(storeId <= 0) return false;

            Store store = await _unitOfWork.StoreRepository.GetByIDAsync(storeId);
            store.Active = !store.Active;

            await _unitOfWork.StoreRepository.UpdateAsync(store);
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<bool> Add(StoreModel model)
        {
            if (model == null) return false;

            await _unitOfWork.StoreRepository.AddAsync(_mapper.Map<Store>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<bool> Delete(StoreModel model)
        {
            if(model == null || model.Id <= 0) return false;

            await _unitOfWork.StoreRepository.DeleteAsync(model.Id);
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<bool> Delete(int id)
        {
            if(id <= 0) return false;

            await _unitOfWork.StoreRepository.DeleteAsync(id);
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<List<StoreModel>> Get(StoreFilter Filters)
        {
            Expression<Func<Store, bool>> _Expression = null;

            if (Filters != null)
            {
                _Expression =
                (
                    x => (!string.IsNullOrEmpty(Filters.Name) ? x.Name.ToLower().Contains(Filters.Name.ToLower()) : true)
                );
            }

            List<Store> stores = await _unitOfWork.StoreRepository.GetAsync(_Expression, o => o.OrderBy(s => s.DisplayOrder)) as List<Store>;

            return _mapper.Map<List<StoreModel>>(stores);
        }

        public async Task<List<StoreModel>> Get(StoreFilter Filters, string includedProps)
        {
            Expression<Func<Store, bool>> _Expression = null;

            if (Filters != null)
            {
                _Expression =
                (
                    x => (!string.IsNullOrEmpty(Filters.Name) ? x.Name.ToLower().Contains(Filters.Name.ToLower()) : true)
                );
            }

            List<Store> stores = await _unitOfWork.StoreRepository.GetAsync(_Expression, o => o.OrderBy(s => s.DisplayOrder), includedProps) as List<Store>;

            return _mapper.Map<List<StoreModel>>(stores);
        }

        public async Task<List<StoreModel>> GetAll()
        {
            return await Get(null);
        }

        public async Task<StoreModel> GetByID(int id)
        {
            if(id == 0) return null;

            Store store = await _unitOfWork.StoreRepository.GetByIDAsync(id);

            return _mapper.Map<StoreModel>(store);
        }

        public async Task<bool> Update(StoreModel model)
        {
            if(model == null) return false;

            await _unitOfWork.StoreRepository.UpdateAsync(_mapper.Map<Store>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }
    }
}
