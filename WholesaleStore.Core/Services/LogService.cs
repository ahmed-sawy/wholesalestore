﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.Services
{
    public class LogService : ILogService
    {
        public Task<bool> Add(LogModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(LogModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<LogModel>> Get(LogFilter Filters)
        {
            throw new NotImplementedException();
        }

        public Task<List<LogModel>> Get(LogFilter Filters, string includedProps)
        {
            throw new NotImplementedException();
        }

        public Task<LogModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(LogModel model)
        {
            throw new NotImplementedException();
        }
    }
}
