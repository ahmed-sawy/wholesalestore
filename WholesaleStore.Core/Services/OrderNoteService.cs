﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.Services
{
    public class OrderNoteService : IOrderNoteService
    {
        public Task<bool> Add(OrderNoteModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(OrderNoteModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<OrderNoteModel>> Get(OrderNoteFilter Filters)
        {
            throw new NotImplementedException();
        }

        public Task<List<OrderNoteModel>> Get(OrderNoteFilter Filters, string includedProps)
        {
            throw new NotImplementedException();
        }

        public Task<OrderNoteModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(OrderNoteModel model)
        {
            throw new NotImplementedException();
        }
    }
}
