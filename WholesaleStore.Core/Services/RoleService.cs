﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RoleService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Add(RoleModel model)
        {
            if(model == null) return false;

            await _unitOfWork.RoleRepository.AddAsync(_mapper.Map<ApplicationRole>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<bool> Delete(RoleModel model)
        {
            if(model == null) return false;

            return await Delete(model.Id);
        }

        public async Task<bool> Delete(int id)
        {
            if(id <= 0) return false;

            await _unitOfWork.RoleRepository.DeleteAsync(id);
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<List<RoleModel>> Get(RoleFilter Filters)
        {
            return await Get(Filters, "");
        }

        public async Task<List<RoleModel>> Get(RoleFilter Filters, string includedProps)
        {
            Expression<Func<ApplicationRole, bool>> _Expression = null;

            if (Filters != null)
            {
                _Expression =
                (
                    x => (!string.IsNullOrEmpty(Filters.Name) ? x.Name.ToLower().Contains(Filters.Name.ToLower()) : true)
                );
            }

            List<ApplicationRole> roles = await _unitOfWork.RoleRepository.GetAsync(_Expression, null, includedProps) as List<ApplicationRole>;
            return _mapper.Map<List<RoleModel>>(roles);
        }

        public async Task<RoleModel> GetByID(int id)
        {
            if (id <= 0) return null;

            ApplicationRole role = await _unitOfWork.RoleRepository.GetByIDAsync(id);
            return _mapper.Map<RoleModel>(role);
        }

        public async Task<bool> Update(RoleModel model)
        {
            if (model == null) return false;

            await _unitOfWork.RoleRepository.UpdateAsync(_mapper.Map<ApplicationRole>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }
    }
}
