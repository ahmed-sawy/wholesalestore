﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.Services
{
    public class ProductPictureService : IProductPictureService
    {
        public Task<bool> Add(ProductPictureModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(ProductPictureModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<ProductPictureModel>> Get(ProductPictureFilter Filters)
        {
            throw new NotImplementedException();
        }

        public Task<List<ProductPictureModel>> Get(ProductPictureFilter Filters, string includedProps)
        {
            throw new NotImplementedException();
        }

        public Task<ProductPictureModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(ProductPictureModel model)
        {
            throw new NotImplementedException();
        }
    }
}
