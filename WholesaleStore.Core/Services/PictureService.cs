﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class PictureService : IPictureService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PictureService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Add(PictureModel model)
        {
            if(model == null) return false;

            await _unitOfWork.PictureRepository.AddAsync(_mapper.Map<Picture>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<bool> Delete(PictureModel model)
        {
            if (model == null) return false;

            return await Delete(model.Id);
        }

        public async Task<bool> Delete(int id)
        {
            if (id <= 0) return false;

            await _unitOfWork.PictureRepository.DeleteAsync(id);
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<List<PictureModel>> Get(PictureFilter Filters)
        {
            return await Get(Filters, "");
        }

        public async Task<List<PictureModel>> Get(PictureFilter Filters, string includedProps)
        {
            List<Picture> pictures = await _unitOfWork.PictureRepository.GetAsync(null, null, includedProps) as List<Picture>;
            return _mapper.Map<List<PictureModel>>(pictures);
        }

        public async Task<PictureModel> GetByID(int id)
        {
            if(id <= 0) return null;

            Picture picture = await _unitOfWork.PictureRepository.GetByIDAsync(id);
            return _mapper.Map<PictureModel>(picture);
        }

        public async Task<bool> Update(PictureModel model)
        {
            if(model == null) return false;

            await _unitOfWork.PictureRepository.UpdateAsync(_mapper.Map<Picture>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }
    }
}
