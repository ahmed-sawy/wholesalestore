﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<OrderService> _logger;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<OrderService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<bool> Add(OrderModel model)
        {
            if (model == null) return false;

            Order order = _mapper.Map<Order>(model);

            order.CreatedOnUtc = DateTime.UtcNow;
            order.OrderGuid = Guid.NewGuid();
            order.User = await _unitOfWork.UserRepository.GetByIDAsync(order.UserId);
            order.OrderStatus = OrderStatus.Pending;
            order.PaymentMethodSystemName = "Cash on delivery";
            order.CustomerCurrencyCode = "EGP";
            //TODO get customer IP 
            //TODO set customer order number

            order.OrderSubtotalExclTax = model.OrderItems.Sum(x => x.PriceExclTax);
            order.OrderSubtotalInclTax = model.OrderItems.Sum(x => x.PriceInclTax);

            order.OrderShippingInclTax = 0;
            order.OrderShippingExclTax = 0;

            order.OrderTotal = order.OrderSubtotalInclTax + order.OrderTax;

            await ConfigOrderItems(order.OrderItems.ToList());

            await _unitOfWork.OrderRepository.AddAsync(order);
            await _unitOfWork.SaveAsync();

            return true;
        }

        private async Task ConfigOrderItems(List<OrderItem> orderItems)
        {
            if (orderItems == null) return;

            foreach (var item in orderItems)
            {
                item.OrderItemGuid = Guid.NewGuid();
                item.Product = await _unitOfWork.ProductRepository.GetByIDAsync(item.ProductId);
                item.PriceExclTax = item.UnitPriceExclTax * item.Quantity;
                item.PriceInclTax = item.UnitPriceInclTax * item.Quantity;
            }
        }

        public Task<bool> Delete(OrderModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<OrderModel>> Get(OrderFilter Filters)
        {
            return await Get(Filters, "");
        }

        public async Task<List<OrderModel>> Get(OrderFilter Filters, string includedProps)
        {
            Expression<Func<Order, bool>> _Expression = null;

            if (Filters != null)
            {
                _Expression =
                (
                    x => (Filters.UserId > -1 ? x.UserId == Filters.UserId : true)
                      && (!string.IsNullOrEmpty(Filters.PaymentMethodSystemName) ? x.PaymentMethodSystemName.ToLower().Contains(Filters.PaymentMethodSystemName.ToLower()) : true)
                      && (Filters.OrderTotal > 0 ? x.OrderTotal == Filters.OrderTotal : true)
                      && (Filters.OrderStatusId > -1 ? x.OrderStatusId == Filters.OrderStatusId : true)
                );
            }

            List<Order> orders = await _unitOfWork.OrderRepository.GetAsync(_Expression, o => o.OrderBy(s => s.CreatedOnUtc), includedProps, Filters.MaxRows, (Filters.CurrentPage - 1) * Filters.MaxRows) as List<Order>;

            int count = await _unitOfWork.OrderRepository.GetCountAsync(_Expression);

            double pageCount = (double)((decimal)count / Convert.ToDecimal(Filters.MaxRows));
            Filters.PageCount = (int)Math.Ceiling(pageCount);
            Filters.CurrentPage = Filters.CurrentPage;

            return _mapper.Map<List<OrderModel>>(orders);
        }

        public Task<OrderModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(OrderModel model)
        {
            throw new NotImplementedException();
        }

        public async Task<OrderModel> ChangeStatus(Guid orderGuid, int statusId)
        {
            try
            {
                Order order = await _unitOfWork.OrderRepository.GetOneAsync(o => o.OrderGuid == orderGuid);

                if (order == null) return null;

                order.OrderStatusId = statusId;
                await _unitOfWork.OrderRepository.UpdateAsync(order);
                await _unitOfWork.SaveAsync();

                return _mapper.Map<OrderModel>(order);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<OrderModel> Cancel(Guid orderGuid)
        {
            try
            {
                Order order = await _unitOfWork.OrderRepository.GetOneAsync(o => o.OrderGuid == orderGuid);

                if(order == null) return null;

                order.OrderStatusId = (int)OrderStatus.Cancelled;
                await _unitOfWork.OrderRepository.UpdateAsync(order);
                await _unitOfWork.SaveAsync();

                return _mapper.Map<OrderModel>(order);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<OrderModel> GetByGuid(Guid orderGuid)
        {
            Order order = await _unitOfWork.OrderRepository.GetOneAsync(u => u.OrderGuid == orderGuid);
            OrderModel orderModel = _mapper.Map<OrderModel>(order);

            return orderModel;
        }
    }
}
