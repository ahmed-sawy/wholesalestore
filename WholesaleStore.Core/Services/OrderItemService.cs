﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class OrderItemService : IOrderItemService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<OrderItemService> _logger;

        public OrderItemService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<OrderItemService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public Task<bool> Add(OrderItemModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(OrderItemModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<OrderItemModel>> Get(OrderItemFilter Filters)
        {
            return await Get(Filters, "");
        }

        public async Task<List<OrderItemModel>> Get(OrderItemFilter Filters, string includedProps)
        {
            Expression<Func<OrderItem, bool>> _Expression = null;

            if (Filters != null)
            {
                _Expression =
                (
                    x => (Filters.OrderId > 0 ? x.OrderId == Filters.OrderId : true)
                );
            }

            List<OrderItem> orderItems = await _unitOfWork.OrderItemRepository.GetAsync(_Expression, o => o.OrderBy(oi => oi.Id), includedProps) as List<OrderItem>;

            return _mapper.Map<List<OrderItemModel>>(orderItems);
        }

        public Task<OrderItemModel> GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<OrderItemModel>> GetByOrderId(int orderId)
        {
            OrderItemFilter orderItemFilter = new OrderItemFilter()
            {
                OrderId = orderId
            };

            return await Get(orderItemFilter, "");
        }

        public Task<bool> Update(OrderItemModel model)
        {
            throw new NotImplementedException();
        }
    }
}
