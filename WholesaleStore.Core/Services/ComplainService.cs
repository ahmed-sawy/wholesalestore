﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Core.Services
{
    public class ComplainService : IComplainService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ComplainService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Add(ComplainModel model)
        {
            if(model == null) return false;

            await _unitOfWork.ComplainRepository.AddAsync(_mapper.Map<Complain>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<bool> Delete(ComplainModel model)
        {
            if (model == null) return false;

            return await Delete(model.Id);
        }

        public async Task<bool> Delete(int id)
        {
            if(id <= 0) return false;

            Complain complain = await _unitOfWork.ComplainRepository.GetByIDAsync(id);

            if(complain == null) return false;

            await _unitOfWork.ComplainRepository.DeleteAsync(complain);
            await _unitOfWork.SaveAsync();

            return true;
        }

        public async Task<List<ComplainModel>> Get(ComplainFilter Filters)
        {
            return await Get(Filters, "");
        }

        public async Task<List<ComplainModel>> Get(ComplainFilter Filters, string includedProps)
        {
            Expression<Func<Complain, bool>> _Expression = null;

            if (Filters != null)
            {
                _Expression =
                (
                    x => (!string.IsNullOrEmpty(Filters.Title) ? x.Title.ToLower().Contains(Filters.Title.ToLower()) : true)
                        && (Filters.UserId > 0 ? x.UserId == Filters.UserId : true)
                );
            }

            List<Complain> complains = await _unitOfWork.ComplainRepository.GetAsync(_Expression, o => o.OrderBy(c => c.CreatedDate), includedProps) as List<Complain>;
            return _mapper.Map<List<ComplainModel>>(complains);
        }

        public async Task<ComplainModel> GetByID(int id)
        {
            if (id <= 0) return null;

            Complain complain = await _unitOfWork.ComplainRepository.GetByIDAsync(id);
            return _mapper.Map<ComplainModel>(complain);
        }

        public async Task<bool> Update(ComplainModel model)
        {
            if(model == null) return false;

            await _unitOfWork.ComplainRepository.UpdateAsync(_mapper.Map<Complain>(model));
            await _unitOfWork.SaveAsync();

            return true;
        }
    }
}
