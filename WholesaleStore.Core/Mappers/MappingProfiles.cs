﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WholesaleStore.Models.Models;
using WholesaleStore.Persistance.Data.Entities;

namespace WholesaleStore.Core.Mappers
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Store, StoreModel>().ReverseMap();

            CreateMap<ApplicationUser, UserModel>()
                //.ForMember(dto => dto.Roles, conf => conf.MapFrom(r => r.UserRoles.Select(ur => ur.Role).ToList()))
                .ReverseMap();

            CreateMap<ApplicationRole, RoleModel>().ReverseMap();
            CreateMap<ApplicationUserRole, UserRoleModel>().ReverseMap();
            CreateMap<Address, AddressModel>().ReverseMap();
            CreateMap<Order, OrderModel>().ReverseMap();
            CreateMap<ReturnRequest, ReturnRequestModel>().ReverseMap();
            CreateMap<Category, CategoryModel>().ReverseMap();
            CreateMap<Product, ProductModel>().ReverseMap();
        }
    }
}
