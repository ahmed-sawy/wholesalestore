﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.IServices
{
    public interface IAuthenticateService
    {
        Task<SignInResult> Authenticate(LoginModel loginModel);
        Task<SignInResult> Authenticate(string email, string password);
        IEnumerable<Claim> GenerateClaims(UserModel userModel);
    }
}
