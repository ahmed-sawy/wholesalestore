﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.IServices
{
    public interface IOrderService : IBaseService<OrderModel, OrderFilter>
    {
        Task<OrderModel> ChangeStatus(Guid orderGuid, int statusId);
        Task<OrderModel> Cancel(Guid orderGuid);
        Task<OrderModel> GetByGuid(Guid orderGuid);
    }
}
