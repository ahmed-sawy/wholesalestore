﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.IServices
{
    public interface IProductService : IBaseService<ProductModel, ProductFilter>
    {
        Task<bool> Add(ProductModel product, List<IFormFile> pictures = null, IFormFile mainPicture = null);
    }
}
