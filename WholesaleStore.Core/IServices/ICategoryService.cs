﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.IServices
{
    public interface ICategoryService : IBaseService<CategoryModel, CategoryFilter>
    {
        Task<List<CategoryModel>> GetAllCategoriesWithSubCategories();
        Task<bool> Activate(int categoryId);
        Task<List<CategoryModel>> GetAllCategories();
    }
}
