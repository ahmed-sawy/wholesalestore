﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.IServices
{
    public interface IStoreService : IBaseService<StoreModel, StoreFilter>
    {
        Task<bool> ActivateStore(int storeId);
        Task<List<StoreModel>> GetAll();
    }
}
