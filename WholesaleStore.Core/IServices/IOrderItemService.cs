﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.IServices
{
    public interface IOrderItemService : IBaseService<OrderItemModel, OrderItemFilter>
    {
        Task<List<OrderItemModel>> GetByOrderId(int orderId);
    }
}
