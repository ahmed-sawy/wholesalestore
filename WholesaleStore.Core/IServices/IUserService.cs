﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.IServices
{
    public interface IUserService : IBaseService<UserModel, UserFilter>
    {
        Task<UserModel> GetByName(string name);
        Task<UserModel> GetByPhoneNumber(string phoneNumber);
        Task<UserModel> GetByEmail(string email);
        Task<UserModel> GetByGuid(Guid userGuid);
        Task<UserModel> GetAdminUserByStoreId(int storeId);
        Task<UserModel> Register(RegisterModel registerModel);
        Task<UserModel> Register(string username, string email, string phoneNumber, string password);
        Task<bool> ResetPassword(string phoneNumber, string password);
        Task<bool> ChangePassword(string phoneNumber, string oldPassword, string newPassword);
        Task<bool> ConfirmPhoneNumber(string phoneNumber);
    }
}
