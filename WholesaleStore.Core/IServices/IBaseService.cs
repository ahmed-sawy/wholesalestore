﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WholesaleStore.Core.IServices
{
    public interface IBaseService<TModel, TFilter> where TModel : class
    {
        Task<TModel> GetByID(int id);
        Task<List<TModel>> Get(TFilter Filters);
        Task<List<TModel>> Get(TFilter Filters, string includedProps);
        Task<bool> Add(TModel model);
        Task<bool> Update(TModel model);
        Task<bool> Delete(TModel model);
        Task<bool> Delete(int id);
    }
}