﻿using System;
using System.Collections.Generic;
using System.Text;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Core.IServices
{
    public interface IProductReviewService : IBaseService<ProductReviewModel, ProductReviewFilter>
    {
    }
}
