﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace WholesaleStore.Core.IServices
{
    public interface ITokenService
    {
        string GenerateToken(IEnumerable<Claim> claims);
        string GenerateRefreshToken();
    }
}
