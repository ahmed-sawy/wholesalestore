﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.ViewModels.VMs
{
    public class ProductVM
    {
        public List<ProductModel> Products { get; set; }
        public ProductModel Product { get; set; }
        public StoreModel Store { get; set; }
        public ProductFilter ProductFilter { get; set; }
        public List<IFormFile> Pictures { get; set; }
        public IFormFile MainPicture { get; set; }
        public List<CategoryModel> Categories { get; set; }
    }
}
