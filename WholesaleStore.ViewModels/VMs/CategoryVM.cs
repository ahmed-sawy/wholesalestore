﻿using System;
using System.Collections.Generic;
using System.Text;
using WholesaleStore.Models.Models;

namespace WholesaleStore.ViewModels.VMs
{
    public class CategoryVM
    {
        public CategoryModel Category { get; set; }
        public List<CategoryModel> Categories { get; set; }
        public List<CategoryModel> MainCategories { get; set; }
    }
}
