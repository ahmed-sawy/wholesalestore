﻿using System;
using System.Collections.Generic;
using System.Text;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.ViewModels.VMs
{
    public class OrderVM
    {
        public AddressModel ShippingAddress { get; set; }
        public StoreModel Store { get; set; }
        public UserModel User { get; set; }
        public OrderFilter OrderFilter { get; set; }
        public List<OrderModel> Orders { get; set; }
    }
}
