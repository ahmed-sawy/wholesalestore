﻿using System;
using System.Collections.Generic;
using System.Text;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.ViewModels.VMs
{
    public class UserVM
    {
        public UserFilter UserFilter { get; set; }
        public List<UserModel> Users { get; set; }
        public List<StoreModel> Stores { get; set; }
    }
}
