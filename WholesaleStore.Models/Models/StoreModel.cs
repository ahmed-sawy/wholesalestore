﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Models
{
    public class StoreModel : BaseModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public bool SslEnabled { get; set; }
        public string Hosts { get; set; }
        public int DefaultLanguageId { get; set; }
        public int DisplayOrder { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhoneNumber { get; set; }
        public string CompanyVat { get; set; }
        public bool Deleted { get; set; }
        public bool Active { get; set; }
        public int AdminUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public UserModel AdminUser { get; set; }
    }
}
