﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Models
{
    public class UserRoleModel
    {
        public int UserId { get; set; }
        public UserModel User { get; set; }

        public int RoleId { get; set; }
        public RoleModel Role { get; set; }
    }
}
