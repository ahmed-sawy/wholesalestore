﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Models
{
    public class PictureModel : BaseModel
    {
        public string Name { get; set; }
        public string VirtualPath { get; set; }
    }
}
