﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Models
{
    public class UserModel : BaseModel
    {
        public Guid UserGuid { get; set; }
        public string ComapnyName { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public string SystemName { get; set; }
        public string Email { get; set; }
        public bool HasShoppingCartItems { get; set; }
        public bool RequireReLogin { get; set; }
        public DateTime? CannotLoginUntilDateUtc { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public string LastIpAddress { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime? LastLoginDateUtc { get; set; }
        public DateTime LastActivityDateUtc { get; set; }
        public int? ShippingAddressId { get; set; }
        public int RegisteredInStoreId { get; set; }
        public string ProfilePicture { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool EmailConfirmed { get; set; }

        public string RefreshToken { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }

        public AddressModel Address { get; set; }
        public List<OrderModel> Orders { get; set; }
        public List<ReturnRequestModel> ReturnRequests { get; set; }
        public List<RoleModel> Roles { get; set; }
    }
}
