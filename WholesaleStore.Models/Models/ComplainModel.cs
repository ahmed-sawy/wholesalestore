﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Models
{
    public class ComplainModel : BaseModel
    {
        public string Title { get; set; }
        public string Desc { get; set; }
        public int UserId { get; set; }
        public DateTime CreatedDate { get; set; }

        public UserModel User { get; set; }
    }
}
