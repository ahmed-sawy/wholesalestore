﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Models
{
    public class OrderModel : BaseModel
    {
        public Guid OrderGuid { get; set; }
        public int StoreId { get; set; }
        public int UserId { get; set; }
        public int BillingAddressId { get; set; }
        public int? ShippingAddressId { get; set; }
        public int OrderStatusId { get; set; }
        public string PaymentMethodSystemName { get; set; }
        public string CustomerCurrencyCode { get; set; }
        public decimal OrderSubtotalInclTax { get; set; }
        public decimal OrderSubtotalExclTax { get; set; }
        public decimal OrderSubTotalDiscountInclTax { get; set; }
        public decimal OrderSubTotalDiscountExclTax { get; set; }
        public decimal OrderShippingInclTax { get; set; }
        public decimal OrderShippingExclTax { get; set; }
        public decimal PaymentMethodAdditionalFeeInclTax { get; set; }
        public decimal PaymentMethodAdditionalFeeExclTax { get; set; }
        public decimal OrderTax { get; set; }
        public decimal OrderDiscount { get; set; }
        public decimal OrderTotal { get; set; }
        public decimal RefundedAmount { get; set; }
        public string CheckoutAttributeDescription { get; set; }
        public string CustomerIp { get; set; }
        public DateTime? PaidDateUtc { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public string CustomOrderNumber { get; set; }

        public StoreModel Store { get; set; }
        public UserModel User { get; set; }
        public List<OrderItemModel> OrderItems { get; set; }
        public List<OrderNoteModel> OrderNotes { get; set; }
    }
}
