﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Filters
{
    public class ComplainFilter
    {
        public int UserId { get; set; }
        public string Title { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
