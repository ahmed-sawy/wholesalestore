﻿using System.Collections.Generic;

namespace WholesaleStore.Models.Filters
{
    public class PagingFilter
    {
        public int CurrentPage { get; set; } = 1;
        public int MaxRows { get; set; } = 3;
        public int PageCount { get; set; }
        public int[] PageSizes { 
            get
            {
                return new int[] { 1, 2, 5, 10 };
            }
        }

        public bool HasPreviousPage
        {
            get
            {
                return (CurrentPage > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (CurrentPage < PageCount);
            }
        }
    }
}