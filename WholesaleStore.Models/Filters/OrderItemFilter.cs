﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Filters
{
    public class OrderItemFilter
    {
        public Guid OrderItemGuid { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
    }
}
