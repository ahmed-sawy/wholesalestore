﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Filters
{
    public class RoleFilter
    {
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}
