﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Filters
{
    public class ProductFilter : PagingFilter
    {
        public int StoreId { get; set; } = -1;
        public string Name { get; set; }
        public int StockQuantity { get; set; } = -1;
        public int OrderMinimumQuantity { get; set; } = -1;
        public int OrderMaximumQuantity { get; set; } = -1;
        public bool NotReturnable { get; set; }
        public decimal Price { get; set; } = -1;
        public decimal OldPrice { get; set; } = -1;
        public decimal ProductCost { get; set; } = -1;
        public bool MarkAsNew { get; set; }
        public DateTime? CreatedOnUtc { get; set; }
        public DateTime? UpdatedOnUtc { get; set; }
    }
}
