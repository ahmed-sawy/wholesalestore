﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Filters
{
    public class StoreFilter
    {
        public string Name { get; set; }
        public bool Deleted { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
