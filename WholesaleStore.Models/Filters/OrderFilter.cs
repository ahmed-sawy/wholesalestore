﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Filters
{
    public class OrderFilter : PagingFilter
    {
        public int StoreId { get; set; }
        public int UserId { get; set; }
        public Guid UserGuid { get; set; }
        public int OrderStatusId { get; set; }
        public string PaymentMethodSystemName { get; set; }
        public decimal OrderTotal { get; set; }
        public DateTime? PaidDateUtc { get; set; }
        public DateTime? CreatedOnUtc { get; set; }
    }
}
