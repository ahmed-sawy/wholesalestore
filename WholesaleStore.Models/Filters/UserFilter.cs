﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Filters
{
    public class UserFilter : PagingFilter
    {
        public string Name { get; set; }
        public int StoreId { get; set; }
        public int Active { get; set; }
        public int Deleted { get; set; }
        public DateTime? CreatedOnUtc { get; set; }
    }
}
