﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Models.Filters
{
    public class UserRoleFilter
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
