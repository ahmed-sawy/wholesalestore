﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;
using WholesaleStore.API.Models;
using WholesaleStore.Core.IServices;

namespace WholesaleStore.API.Controllers
{
    //[Route("api/[controller]")]
    public class RegisterController : BaseApiController
    {
        private readonly IUserService _userService;

        public RegisterController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("")]
        public async Task<IActionResult> Registration(RegisterModel registerModel)
        {
            try
            {
                var result = await _userService.Register(registerModel.Username, registerModel.Email, registerModel.PhoneNumber, registerModel.Password);
                if (result != null)
                {
                    return Ok(new ApiResponseModel()
                    {
                        ResponseCode = (int)HttpStatusCode.OK,
                        ResponseMessage = "user saved successufully, please confirm phone number to complete registration process",
                        Result = new
                        {
                            user = new
                            {
                                MarketName = result.ComapnyName,
                                UserGuid = result.UserGuid,
                                PhoneNumber = result.PhoneNumber,
                                Name = result.SystemName,
                                Active = result.Active,
                                ProfilePicture = result.ProfilePicture,
                                PhoneNumberConfirmed = result.PhoneNumberConfirmed
                            }
                        }
                    });
                }

                return BadRequest(new ApiResponseModel()
                {
                    ResponseCode = (int)HttpStatusCode.BadRequest,
                    ResponseMessage = "something wrong happen, or phone number already exists",
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
