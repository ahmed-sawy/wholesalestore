﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WholesaleStore.API.Models;
using WholesaleStore.Core.IServices;

namespace WholesaleStore.API.Controllers
{
    //[Route("api/[controller]")]
    public class LoginController : BaseApiController
    {
        private readonly IAuthenticateService _authenticateService;
        private readonly ILogger<LoginController> _logger;
        private readonly IUserService _userService;
        private readonly ITokenService _tokenService;
        private readonly IConfiguration _config;

        public LoginController(
            IAuthenticateService authenticateService,
            ILogger<LoginController> logger, IUserService userService, 
            ITokenService tokenService, IConfiguration config)
        {
            _authenticateService = authenticateService;
            _logger = logger;
            _userService = userService;
            _tokenService = tokenService;
            _config = config;
        }

        [HttpPost("")]
        public async Task<IActionResult> Authenticate(LoginModel loginModel)
        {

            try
            {
                var user = await _userService.GetByEmail(loginModel.Email);
                if (user == null) return NotFound("Can't find a user with this email");

                var result = await _authenticateService.Authenticate(loginModel.Email, loginModel.Password);
                if (result != null && result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");

                    //var user = await _userManager.FindByEmailAsync(loginModel.Email);
                    string token = _tokenService.GenerateToken(_authenticateService.GenerateClaims(new WholesaleStore.Models.Models.UserModel() { SystemName = user.SystemName, Id = user.Id, Email = user.Email, PhoneNumber = user.PhoneNumber }));
                    string refreshToken = _tokenService.GenerateRefreshToken();

                    _ = int.TryParse(_config["Jwt:RefreshTokenLifetimeInMinutes"], out int refreshTokenLifetimeInMinutes);

                    //user.RefreshToken = refreshToken;
                    //user.RefreshTokenExpiryTime = DateTime.Now.AddMinutes(refreshTokenLifetimeInMinutes);

                    ////await _userManager.UpdateAsync(user);
                    //await _userService.Update(user);

                    return Ok(new
                    {
                        user = new
                        {
                            MarketName = user.ComapnyName,
                            UserGuid = user.UserGuid,
                            Name = user.SystemName,
                            Active = user.Active,
                            ProfilePicture = user.ProfilePicture,
                            Email = user.Email,
                            EmailConfirmed = user.EmailConfirmed
                        },
                        token = token,
                        refreshToken = refreshToken
                    });
                }
                return Ok(new
                {
                    user = new
                    {
                        MarketName = user.ComapnyName,
                        UserGuid = user.UserGuid,
                        Name = user.SystemName,
                        Active = user.Active,
                        ProfilePicture = user.ProfilePicture,
                        Email = user.Email,
                        EmailConfirmed = user.EmailConfirmed
                    }
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("refresh-token")]
        private async Task<IActionResult> RefreshToken(WholesaleStore.Models.Models.TokenModel tokenModel)
        {
            try
            {
                if (tokenModel is null)
                {
                    return BadRequest("Invalid client request");
                }

                string? accessToken = tokenModel.AccessToken;
                string? refreshToken = tokenModel.RefreshToken;

                var principal = GetPrincipalFromExpiredToken(accessToken);
                if (principal == null)
                {
                    return BadRequest("Invalid access token or refresh token");
                }

                string username = principal.Identity.Name;

                var user = await _userService.GetByName(username);

                if (user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
                {
                    return BadRequest("Invalid access token or refresh token, Please login again");
                }

                var newAccessToken = _tokenService.GenerateToken(principal.Claims.ToList());
                var newRefreshToken = _tokenService.GenerateRefreshToken();

                user.RefreshToken = newRefreshToken;
                await _userService.Update(user);

                return Ok(new { token = newAccessToken, refreshToken = newRefreshToken });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private ClaimsPrincipal? GetPrincipalFromExpiredToken(string? token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"])),
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
            if (securityToken is not JwtSecurityToken jwtSecurityToken)
                throw new SecurityTokenException("Invalid token");

            return principal;

        }
    }
}
