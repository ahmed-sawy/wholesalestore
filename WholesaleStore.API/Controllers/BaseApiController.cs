﻿using Microsoft.AspNetCore.Mvc;
using WholesaleStore.Infrastructure.CustomFilters;

namespace WholesaleStore.API.Controllers
{
    [ServiceFilter(typeof(CustomeExceptionFilter))]
    [Route("api/[controller]")]
    [ApiController]
    public class BaseApiController : ControllerBase
    {
    }
}
