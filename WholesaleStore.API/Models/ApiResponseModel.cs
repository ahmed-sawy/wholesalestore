﻿namespace WholesaleStore.API.Models
{
    public class ApiResponseModel
    {
        public string ResponseMessage { get; set; }
        public int ResponseCode { get; set; }
        public object Result { get; set; }
    }
}
