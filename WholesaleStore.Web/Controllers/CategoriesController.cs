﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Infrastructure.CustomFilters;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.ViewModels.VMs;

namespace WholesaleStore.Web.Controllers
{
    public class CategoriesController : BaseController
    {
        private readonly ICategoryService _categoryService;
        private readonly ILogger<CategoriesController> _logger;

        public CategoriesController(ICategoryService categoryService, ILogger<CategoriesController> logger)
        {
            _categoryService = categoryService;
            _logger = logger;
        }

        [Authorize(Roles.SUPER_ADMIN)]
        public async Task<IActionResult> Index()
        {
            CategoryFilter filter = new CategoryFilter();

            List<CategoryModel> categories = await _categoryService.GetAllCategories();

            return View(categories);
        }

        public async Task<IActionResult> Create()
        {
            try
            {
                CategoryFilter categoryFilter = new CategoryFilter();
                categoryFilter.ParentCategoryId = 0;

                CategoryVM categoryVM = new CategoryVM();
                categoryVM.Category = new CategoryModel();

                categoryVM.MainCategories = await _categoryService.Get(categoryFilter);
                //categoryVM.Categories = await _categoryService.GetAllCategoriesWithSubCategories();
                return View(categoryVM);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create(CategoryModel category)
        {
            try
            {
                bool succeded = await _categoryService.Add(category);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IActionResult> ActivateCategory(int GategoryId)
        {
            //try
            //{
            //    bool succeded = await _seasonService.ActivateSeason(seasonId);
            //    if (!succeded) TempData["ErrorMsg"] = "Something wrong";
            //    return RedirectToAction("Index", new { schoolId = schoolId });
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}
            return RedirectToAction("Index");
        }
    }
}
