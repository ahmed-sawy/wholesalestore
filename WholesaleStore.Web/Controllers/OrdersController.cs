﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.ViewModels.VMs;

namespace WholesaleStore.Web.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly ILogger<OrdersController> _logger;

        public OrdersController(IOrderService orderService, ILogger<OrdersController> logger)
        {
            _orderService = orderService;
            _logger = logger;
        }

        public async Task<IActionResult> Index(int storeId, int userId, Guid userGuid,
            int orderStatusId = -1, string paymentMethodName = "", decimal orderTotal = 0,
            DateTime? paidDate = null , DateTime? createdDate = null,
            int currentPage = 1, int maxRows = 2)
        {
            ViewData["MaxRows"] = maxRows;

            OrderVM orderVM = new OrderVM();
            orderVM.OrderFilter = new OrderFilter();
            orderVM.OrderFilter.CurrentPage = currentPage;
            orderVM.OrderFilter.MaxRows = maxRows;
            orderVM.OrderFilter.CreatedOnUtc = createdDate;
            orderVM.OrderFilter.PaidDateUtc = paidDate;
            orderVM.OrderFilter.StoreId = storeId;
            orderVM.OrderFilter.UserId = userId;
            orderVM.OrderFilter.UserGuid = userGuid;
            orderVM.OrderFilter.OrderStatusId = orderStatusId;
            orderVM.OrderFilter.PaymentMethodSystemName = paymentMethodName;
            orderVM.OrderFilter.OrderTotal = orderTotal;

            orderVM.Orders = await _orderService.Get(orderVM.OrderFilter);

            return View(orderVM);
        }
    }
}
