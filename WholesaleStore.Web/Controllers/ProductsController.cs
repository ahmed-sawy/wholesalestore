﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.ViewModels.VMs;

namespace WholesaleStore.Web.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly ILogger<ProductsController> _logger;

        public ProductsController(IProductService productService, ILogger<ProductsController> logger, ICategoryService categoryService)
        {
            _productService = productService;
            _logger = logger;
            _categoryService = categoryService;
        }

        public async Task<IActionResult> Index(
            int storeId, string name = "",
            int stockQuantity = 0, decimal price = 0,
            decimal cost = 0, DateTime? created = null, DateTime? updated = null,
            int currentPage = 1, int maxRows = 2)
        {
            ViewData["MaxRows"] = maxRows;

            ProductVM productVM = new ProductVM();
            productVM.ProductFilter = new ProductFilter();
            productVM.ProductFilter.CurrentPage = currentPage;
            productVM.ProductFilter.MaxRows = maxRows;
            productVM.ProductFilter.StoreId = storeId;
            productVM.ProductFilter.Name = name;
            productVM.ProductFilter.StockQuantity = stockQuantity;
            productVM.ProductFilter.Price = price;
            productVM.ProductFilter.ProductCost = cost;
            productVM.ProductFilter.CreatedOnUtc = created;
            productVM.ProductFilter.UpdatedOnUtc = updated;

            productVM.Products = await _productService.Get(productVM.ProductFilter);

            return View(productVM);
        }

        public async Task<IActionResult> Create(int storeId)
        {
            try
            {
                ProductVM productVM = new ProductVM();
                productVM.Product = new ProductModel();
                productVM.Product.StoreId = storeId;
                productVM.Product.MainCategory = new CategoryModel();
                productVM.Categories = await _categoryService.GetAllCategoriesWithSubCategories();
                return View(productVM);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create(ProductVM productVM)
        {
            try
            {
                bool succeded = await _productService.Add(productVM.Product, productVM.Pictures, productVM.MainPicture);
                //if (!succeded)

                return RedirectToAction("Index", new { storeId = productVM.Product.StoreId });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<CategoryModel>> GetChildCategories(int categoryId)
        {
            return await _categoryService.Get(new CategoryFilter() { ParentCategoryId = categoryId });
        }
    }
}
