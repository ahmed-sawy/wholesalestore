﻿using Microsoft.AspNetCore.Mvc;
using WholesaleStore.Infrastructure.CustomFilters;

namespace WholesaleStore.Web.Controllers
{
    [ServiceFilter(typeof(CustomeExceptionFilter))]
    public class BaseController : Controller
    {
    }
}
