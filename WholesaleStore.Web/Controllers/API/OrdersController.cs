﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.Web.Controllers.API.ApiModels;

namespace WholesaleStore.Web.Controllers.API
{
    public class OrdersController : BaseApiController
    {
        private readonly ILogger<OrdersController> _logger;
        private readonly IOrderService _orderService;
        private readonly IOrderItemService _orderItemService;
        private readonly IUserService _userService;

        public OrdersController(
            ILogger<OrdersController> logger, IOrderService orderService, 
            IUserService userService, IOrderItemService orderItemService)
        {
            _logger = logger;
            _orderService = orderService;
            _userService = userService;
            _orderItemService = orderItemService;
        }

        [HttpGet("{userGuid}")]
        public async Task<IActionResult> GetOrders(
            Guid userGuid, int storeId = 0, int orderStatusId = -1,
            string paymentMethodSystemName = "", decimal orderTotal = 0,
            DateTime? paidDateUtc = null, DateTime? createdOnUtc = null,
            int currentPage = 1, int maxRows = 10)
        {
            try
            {
                var user = await _userService.GetByGuid(userGuid);

                if (user == null)
                {
                    return BadRequest(new ApiResponseModel()
                    {
                        ResponseCode = (int)HttpStatusCode.BadRequest,
                        ResponseMessage = "something wrong happen, this user does not exist",
                    });
                }

                OrderFilter filter = new OrderFilter()
                {
                    UserId = user.Id,
                    UserGuid = userGuid,
                    StoreId = storeId,
                    OrderStatusId = orderStatusId,
                    PaymentMethodSystemName = paymentMethodSystemName,
                    OrderTotal = orderTotal,
                    PaidDateUtc = paidDateUtc,
                    CreatedOnUtc = createdOnUtc,
                    CurrentPage = currentPage,
                    MaxRows = maxRows
                };

                var result = await _orderService.Get(filter);

                if (result == null)
                {
                    return BadRequest(new ApiResponseModel()
                    {
                        ResponseCode = (int)HttpStatusCode.BadRequest,
                        ResponseMessage = "something wrong happen, We can not retrive Orders",
                    });
                }

                return Ok(new ApiResponseModel()
                {
                    ResponseCode = (int)HttpStatusCode.OK,
                    ResponseMessage = "Orders retrived successufully",
                    Result = new
                    {
                        orders = result
                    }
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> GetOrders(OrderFilter orderFilter)
        {
            try
            {
                var result = await _orderService.Get(orderFilter);

                if (result == null)
                {
                    return BadRequest(new ApiResponseModel()
                    {
                        ResponseCode = (int)HttpStatusCode.BadRequest,
                        ResponseMessage = "something wrong happen, We can not retrive Orders",
                    });
                }

                return Ok(new ApiResponseModel()
                {
                    ResponseCode = (int)HttpStatusCode.OK,
                    ResponseMessage = "Orders retrived successufully",
                    Result = new
                    {
                        orders = result
                    }
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("{orderGuid}/items", Name = "GetOrderItems")]
        public async Task<IActionResult> GetOrderItems(Guid orderGuid)
        {
            try
            {
                var order = await _orderService.GetByGuid(orderGuid);

                if (order == null)
                {
                    return BadRequest(new ApiResponseModel()
                    {
                        ResponseCode = (int)HttpStatusCode.BadRequest,
                        ResponseMessage = "something wrong happen, We can not find this order",
                    });
                }

                List<OrderItemModel> items = await _orderItemService.GetByOrderId(order.Id);

                return Ok(new ApiResponseModel()
                {
                    ResponseCode = (int)HttpStatusCode.OK,
                    ResponseMessage = "Order Items retrived successufully",
                    Result = new
                    {
                        order = order,
                        orderItems = items
                    }
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("{orderGuid}/Cancel")]
        public async Task<IActionResult> CancelOrder(Guid orderGuid)
        {
            try
            {
                var result = await _orderService.Cancel(orderGuid);

                if (result == null)
                {
                    return BadRequest(new ApiResponseModel()
                    {
                        ResponseCode = (int)HttpStatusCode.BadRequest,
                        ResponseMessage = "something wrong happen, We can not cancel Order",
                    });
                }

                return Ok(new ApiResponseModel()
                {
                    ResponseCode = (int)HttpStatusCode.OK,
                    ResponseMessage = "Order canceled successufully",
                    Result = new
                    {
                        order = result
                    }
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("{orderGuid}/ChangeStatus/{statusId}")]
        public async Task<IActionResult> ChangeOrderStatus(Guid orderGuid, int statusId)
        {
            try
            {
                var result = await _orderService.ChangeStatus(orderGuid, statusId);

                if (result == null)
                {
                    return BadRequest(new ApiResponseModel()
                    {
                        ResponseCode = (int)HttpStatusCode.BadRequest,
                        ResponseMessage = "something wrong happen, We can not change Orderu status",
                    });
                }

                return Ok(new ApiResponseModel()
                {
                    ResponseCode = (int)HttpStatusCode.OK,
                    ResponseMessage = "Order status changed successufully",
                    Result = new
                    {
                        order = result
                    }
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
