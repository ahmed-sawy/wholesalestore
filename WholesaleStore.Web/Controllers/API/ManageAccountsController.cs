﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Web.Controllers.API.ApiModels;

namespace WholesaleStore.Web.Controllers.API
{
    public class ManageAccountsController : BaseApiController
    {
        private readonly ILogger<LoginController> _logger;
        private readonly IUserService _userService;

        public ManageAccountsController(ILogger<LoginController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            try
            {
                bool succeded = await _userService.ResetPassword(resetPasswordModel.PhoneNumber, resetPasswordModel.Password);

                if (!succeded) return BadRequest();

                _logger.LogInformation("User reset password");

                return Ok(new ApiResponseModel()
                {
                    ResponseCode = (int)HttpStatusCode.OK,
                    ResponseMessage = "Password reseted successfully"
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("change-password")]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel changePasswordModel)
        {
            try
            {
                bool succeded = await _userService.ChangePassword(changePasswordModel.PhoneNumber, changePasswordModel.OldPassword, changePasswordModel.NewPassword);

                if (!succeded) return BadRequest();

                _logger.LogInformation("User changed password");

                return Ok(new ApiResponseModel()
                {
                    ResponseCode = (int)HttpStatusCode.OK,
                    ResponseMessage = "Password changed successfully"
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("confirm-phonenumber")]
        public async Task<IActionResult> ConfirmPhoneNumber(string phoneNumber)
        {
            try
            {
                bool succeded = await _userService.ConfirmPhoneNumber(phoneNumber);

                if (!succeded) return BadRequest();

                _logger.LogInformation("User confirmed phone number");

                return Ok(new ApiResponseModel()
                {
                    ResponseCode = (int)HttpStatusCode.OK,
                    ResponseMessage = "Phone number confirmed successfully"
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
