﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WholesaleStore.Infrastructure.CustomFilters;

namespace WholesaleStore.Web.Controllers.API
{
    [ServiceFilter(typeof(CustomeExceptionFilter))]
    [Route("api/[controller]")]
    [ApiController]
    public class BaseApiController : ControllerBase
    {
    }
}
