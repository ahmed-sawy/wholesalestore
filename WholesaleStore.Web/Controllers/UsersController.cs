﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Filters;
using WholesaleStore.Models.Models;
using WholesaleStore.ViewModels.VMs;

namespace WholesaleStore.Web.Controllers
{
    public class UsersController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IStoreService _storeService;

        public UsersController(IUserService userService, IStoreService storeService)
        {
            _userService = userService;
            _storeService = storeService;
        }

        public async Task<IActionResult> Index(
            string sortOrder, 
            string currentFilter, 
            string searchString, 
            int currentPage = 1, int maxRows = 3,
            string name = null, DateTime? created = null,
            int active = -1, int deleted = -1, string searchBtn = null)
        {
            ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["EmailSortParm"] = sortOrder == "Email" ? "email_desc" : "Email";

            UserVM userVM = new UserVM();
            userVM.UserFilter = new UserFilter();

            if (searchBtn != null && searchBtn == "Search")
            {
                userVM.UserFilter.CurrentPage = 1;
            }

            userVM.UserFilter.Name = name;
            userVM.UserFilter.CurrentPage = currentPage;
            userVM.UserFilter.MaxRows = maxRows;
            userVM.UserFilter.CreatedOnUtc = created;
            userVM.UserFilter.Active = active;
            userVM.UserFilter.Deleted = deleted;

            userVM.Users = await _userService.Get(userVM.UserFilter);

            userVM.Stores = await _storeService.GetAll();

            switch (sortOrder)
            {
                case "name_desc":
                    userVM.Users = userVM.Users.OrderByDescending(s => s.Username).ToList();
                    break;
                case "Email":
                    userVM.Users = userVM.Users.OrderBy(s => s.Email).ToList();
                    break;
                case "email_desc":
                    userVM.Users = userVM.Users.OrderByDescending(s => s.Email).ToList();
                    break;
                default:
                    userVM.Users = userVM.Users.OrderBy(s => s.Username).ToList();
                    break;
            }

            return View(userVM);
        }
    }
}
