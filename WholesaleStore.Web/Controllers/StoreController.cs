﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WholesaleStore.Core.IServices;
using WholesaleStore.Infrastructure.CustomFilters;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Web.Controllers
{
    public class StoreController : BaseController
    {
        private readonly IStoreService _storeService;
        private readonly IUserService _userService;

        public StoreController(IStoreService storeService, IUserService userService)
        {
            _storeService = storeService;
            _userService = userService;
        }

        [Authorize(Roles.SUPER_ADMIN)]
        public async Task<IActionResult> Index()
        {
            List<StoreModel> stores = await _storeService.Get(null);
            foreach (StoreModel store in stores)
            {
                store.AdminUser = await _userService.GetByID(store.AdminUserId);
            }
            return View(stores);
        }

        public IActionResult Create()
        {
            try
            {
                return View(new StoreModel());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create(StoreModel model)
        {
            try
            {
                model.CreatedDate = DateTime.Now;
                model.UpdatedDate = model.CreatedDate;

                await _storeService.Add(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IActionResult> Edit(int storeId)
        {
            try
            {
                StoreModel store = await _storeService.GetByID(storeId);
                return View(store);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(StoreModel model)
        {
            try
            {
                model.UpdatedDate = DateTime.Now;

                bool updated = await _storeService.Update(model);

                if (!updated)
                    TempData["ErrorMsg"] = "لم يتم التعديل بنجاح";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IActionResult> Delete(int storeId)
        {
            try
            {
                bool deleted = await _storeService.Delete(storeId);
                if (!deleted)
                    TempData["ErrorMsg"] = "لم يتم مسح المخزن بنجاح من فضلك حاول مرة اخري";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IActionResult> ActivateStore(int storeId)
        {
            try
            {
                bool activated = await _storeService.ActivateStore(storeId);

                if (!activated)
                    TempData["ErrorMsg"] = "حدث خطأ ما برجاء المحاولة مرة اخري";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
