﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(WholesaleStore.Web.Areas.Identity.IdentityHostingStartup))]
namespace WholesaleStore.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}