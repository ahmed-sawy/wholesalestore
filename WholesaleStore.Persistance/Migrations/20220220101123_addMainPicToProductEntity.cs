﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WholesaleStore.Persistance.Migrations
{
    public partial class addMainPicToProductEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MainPicture",
                table: "Products",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MainPicture",
                table: "Products");
        }
    }
}
