﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WholesaleStore.Persistance.Migrations
{
    public partial class updateProductEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AllowAddingOnlyExistingAttributeCombinations",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "AllowBackInStockSubscriptions",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "AllowedQuantities",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "AutomaticallyAddRequiredProducts",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "AvailableForPreOrder",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "BackorderModeId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CallForPrice",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CustomerEntersPrice",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "DownloadActivationTypeId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "DownloadExpirationDays",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "DownloadId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "GiftCardTypeId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Gtin",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "HasSampleDownload",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "HasUserAgreement",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "IsDownload",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "IsGiftCard",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "IsRecurring",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "IsRental",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "IsTelecommunicationsOrBroadcastingOrElectronicServices",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "LimitedToStores",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "LowStockActivityId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ManageInventoryMethodId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ManufacturerPartNumber",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "MaxNumberOfDownloads",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "MaximumCustomerEnteredPrice",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "MinimumCustomerEnteredPrice",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "NotifyAdminForQuantityBelow",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "OverriddenGiftCardAmount",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "PreOrderAvailabilityStartDateTimeUtc",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ProductAvailabilityRangeId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "RecurringCycleLength",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "RecurringCyclePeriodId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "RecurringTotalCycles",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "RentalPriceLength",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "RentalPricePeriodId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "RequireOtherProducts",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "RequiredProductIds",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "SampleDownloadId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ShipSeparately",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Sku",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "SubjectToAcl",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "UnlimitedDownloads",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "UseMultipleWarehouses",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "UserAgreementText",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "VendorId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "WarehouseId",
                table: "Products");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AllowAddingOnlyExistingAttributeCombinations",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AllowBackInStockSubscriptions",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "AllowedQuantities",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AutomaticallyAddRequiredProducts",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AvailableForPreOrder",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "BackorderModeId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "CallForPrice",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "CustomerEntersPrice",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "DownloadActivationTypeId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DownloadExpirationDays",
                table: "Products",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DownloadId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "GiftCardTypeId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Gtin",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasSampleDownload",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "HasUserAgreement",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDownload",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsGiftCard",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsRecurring",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsRental",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsTelecommunicationsOrBroadcastingOrElectronicServices",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "LimitedToStores",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "LowStockActivityId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ManageInventoryMethodId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ManufacturerPartNumber",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaxNumberOfDownloads",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "MaximumCustomerEnteredPrice",
                table: "Products",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "MinimumCustomerEnteredPrice",
                table: "Products",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "NotifyAdminForQuantityBelow",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "OverriddenGiftCardAmount",
                table: "Products",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PreOrderAvailabilityStartDateTimeUtc",
                table: "Products",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProductAvailabilityRangeId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RecurringCycleLength",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RecurringCyclePeriodId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RecurringTotalCycles",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RentalPriceLength",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RentalPricePeriodId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "RequireOtherProducts",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "RequiredProductIds",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SampleDownloadId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "ShipSeparately",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Sku",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SubjectToAcl",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UnlimitedDownloads",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseMultipleWarehouses",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UserAgreementText",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VendorId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WarehouseId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
