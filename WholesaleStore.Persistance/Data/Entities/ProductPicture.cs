﻿namespace WholesaleStore.Persistance.Data.Entities
{
    public partial class ProductPicture
    {
        public int ProductId { get; set; }
        public int PictureId { get; set; }
        public int DisplayOrder { get; set; }

        public Product Product { get; set; }
        public Picture Picture { get; set; }
    }
}
