﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Persistance.Data.Entities
{
    public class UserAddress
    {
        public int UserId { get; set; }
        public ApplicationUser User { get; set; }

        public int AddressId { get; set; }
        public Address Address { get; set; }
    }
}
