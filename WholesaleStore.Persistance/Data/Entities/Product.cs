﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
//using Nop.Core.Domain.Common;
//using Nop.Core.Domain.Discounts;
//using Nop.Core.Domain.Localization;
//using Nop.Core.Domain.Security;
//using Nop.Core.Domain.Seo;
//using Nop.Core.Domain.Stores;

namespace WholesaleStore.Persistance.Data.Entities
{
    /// <summary>
    /// Represents a product
    /// </summary>
    public partial class Product : BaseEntity
    {
        public int ProductTypeId { get; set; }

        [ForeignKey("Store")]
        public int StoreId { get; set; }
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        public int ParentGroupedProductId { get; set; }
        public bool VisibleIndividually { get; set; }
        public string Name { get; set; }
        public string MainPicture { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public string AdminComment { get; set; }
        public int ProductTemplateId { get; set; }
        public bool ShowOnHomepage { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public bool AllowCustomerReviews { get; set; }
        public int ApprovedRatingSum { get; set; }
        public int NotApprovedRatingSum { get; set; }
        public int ApprovedTotalReviews { get; set; }
        public int NotApprovedTotalReviews { get; set; }
        public bool IsShipEnabled { get; set; }
        public bool IsFreeShipping { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal AdditionalShippingCharge { get; set; }
        public int DeliveryDateId { get; set; }
        public bool IsTaxExempt { get; set; }
        public int TaxCategoryId { get; set; }
        public int StockQuantity { get; set; }
        public bool DisplayStockAvailability { get; set; }
        public bool DisplayStockQuantity { get; set; }
        public int MinStockQuantity { get; set; }
        public int OrderMinimumQuantity { get; set; }
        public int OrderMaximumQuantity { get; set; }
        public bool NotReturnable { get; set; }
        public bool DisableBuyButton { get; set; }
        public bool DisableWishlistButton { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }

        [Column(TypeName = "decimal(18,2)")] 
        public decimal OldPrice { get; set; }

        [Column(TypeName = "decimal(18,2)")] 
        public decimal ProductCost { get; set; }
        public bool BasepriceEnabled { get; set; }

        [Column(TypeName = "decimal(18,2)")] 
        public decimal BasepriceAmount { get; set; }
        public int BasepriceUnitId { get; set; }

        [Column(TypeName = "decimal(18,2)")] 
        public decimal BasepriceBaseAmount { get; set; }
        public int BasepriceBaseUnitId { get; set; }
        public bool MarkAsNew { get; set; }
        public DateTime? MarkAsNewStartDateTimeUtc { get; set; }
        public DateTime? MarkAsNewEndDateTimeUtc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this product has tier prices configured
        /// <remarks>The same as if we run TierPrices.Count > 0
        /// We use this property for performance optimization:
        /// if this property is set to false, then we do not need to load tier prices navigation property
        /// </remarks>
        /// </summary>
        public bool HasTierPrices { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this product has discounts applied
        /// <remarks>The same as if we run AppliedDiscounts.Count > 0
        /// We use this property for performance optimization:
        /// if this property is set to false, then we do not need to load Applied Discounts navigation property
        /// </remarks>
        /// </summary>
        public bool HasDiscountsApplied { get; set; }

        [Column(TypeName = "decimal(18,2)")] 
        public decimal Weight { get; set; }

        [Column(TypeName = "decimal(18,2)")] 
        public decimal Length { get; set; }

        [Column(TypeName = "decimal(18,2)")] 
        public decimal Width { get; set; }

        [Column(TypeName = "decimal(18,2)")] 
        public decimal Height { get; set; }
        public DateTime? AvailableStartDateTimeUtc { get; set; }
        public DateTime? AvailableEndDateTimeUtc { get; set; }

        /// <summary>
        /// Gets or sets a display order.
        /// This value is used when sorting associated products (used with "grouped" products)
        /// This value is used when sorting home page products
        /// </summary>
        public int DisplayOrder { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }

        public Store Store { get; set; }
        public Category Category { get; set; }
        //public ICollection<ProductCategory> ProductCategories { get; set; }
        public ICollection<ProductPicture> ProductPictures { get; set; }
        public ICollection<ProductReview> ProductReviews { get; set; }
    }
}