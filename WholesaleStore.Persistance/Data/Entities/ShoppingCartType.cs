namespace WholesaleStore.Persistance.Data.Entities
{
    public enum ShoppingCartType
    {
        ShoppingCart = 1,

        Wishlist = 2
    }
}
