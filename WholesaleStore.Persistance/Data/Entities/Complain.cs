﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WholesaleStore.Persistance.Data.Entities
{
    public class Complain : BaseEntity
    {
        public string Title { get; set; }
        public string Desc { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        public DateTime CreatedDate { get; set; }

        public ApplicationUser User { get; set; }
    }
}
