﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Persistance.Data.Entities
{
    public class ApplicationRole : IdentityRole<int>
    {
        public bool Active { get; set; }

        public ICollection<ApplicationUserRole> UserRoles { get; set; }
    }
}
