﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WholesaleStore.Persistance.Data.Entities
{
    public class Address : BaseEntity
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        [Column(TypeName = "decimal(18,2)")] 
        public decimal Latitude { get; set; }
        [Column(TypeName = "decimal(18,2)")] 
        public decimal Longitude { get; set; }

        public ICollection<UserAddress> UserAddresses { get; set; }
    }
}
