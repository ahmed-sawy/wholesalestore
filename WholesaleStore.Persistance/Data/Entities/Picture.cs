﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Persistance.Data.Entities
{
    public class Picture : BaseEntity
    {
        public string Name { get; set; }
        public string VirtualPath { get; set; }
        public ICollection<ProductPicture> ProductPictures { get; set; }
    }
}
