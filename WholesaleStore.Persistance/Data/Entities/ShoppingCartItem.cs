﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WholesaleStore.Persistance.Data.Entities
{
    public partial class ShoppingCartItem : BaseEntity
    {
        [ForeignKey("Store")]
        public int StoreId { get; set; }
        public int ShoppingCartTypeId { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public string AttributesXml { get; set; }
        [Column(TypeName = "decimal(18,2)")] 
        public decimal CustomerEnteredPrice { get; set; }
        public int Quantity { get; set; }
        public DateTime? RentalStartDateUtc { get; set; }
        public DateTime? RentalEndDateUtc { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
        public ShoppingCartType ShoppingCartType
        {
            get => (ShoppingCartType)ShoppingCartTypeId;
            set => ShoppingCartTypeId = (int)value;
        }

        public Store Store { get; set; }
        public ApplicationUser User { get; set; }
        public Product Product { get; set; }
    }
}
