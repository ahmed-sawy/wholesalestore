﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Persistance.Data.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
