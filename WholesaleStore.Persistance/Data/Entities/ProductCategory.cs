﻿namespace WholesaleStore.Persistance.Data.Entities
{
    public partial class ProductCategory
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public bool IsFeaturedProduct { get; set; }
        public int DisplayOrder { get; set; }

        public Product Product { get; set; }
        public Category Category { get; set; }
    }
}
