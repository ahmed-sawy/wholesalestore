﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WholesaleStore.Persistance.Data.Entities
{
    public partial class ReturnRequest : BaseEntity
    {
        public string CustomNumber { get; set; }
        [ForeignKey("Store")]
        public int StoreId { get; set; }
        public int OrderItemId { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        public int Quantity { get; set; }
        public int ReturnedQuantity { get; set; }
        public string ReasonForReturn { get; set; }
        public string RequestedAction { get; set; }
        public string CustomerComments { get; set; }
        public int UploadedFileId { get; set; }
        public string StaffNotes { get; set; }
        public int ReturnRequestStatusId { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
        public ReturnRequestStatus ReturnRequestStatus
        {
            get => (ReturnRequestStatus)ReturnRequestStatusId;
            set => ReturnRequestStatusId = (int)value;
        }

        public Store Store { get; set; }
        public ApplicationUser User { get; set; }
    }
}
