﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WholesaleStore.Persistance.Data.Entities;

namespace WholesaleStore.Persistance.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, IdentityUserClaim<int>,
                                                          ApplicationUserRole, IdentityUserLogin<int>,
                                                          IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<ProductCategory>().HasKey(pc => new { pc.CategoryId, pc.ProductId });
            //modelBuilder.Entity<ProductCategory>()
            //    .HasOne<Category>(c => c.Category)
            //    .WithMany(p => p.ProductCategories)
            //    .HasForeignKey(c => c.CategoryId);
            //modelBuilder.Entity<ProductCategory>()
            //    .HasOne<Product>(p => p.Product)
            //    .WithMany(p => p.ProductCategories)
            //    .HasForeignKey(p => p.ProductId);


            modelBuilder.Entity<ProductPicture>().HasKey(pp => new { pp.ProductId, pp.PictureId });
            modelBuilder.Entity<ProductPicture>()
                .HasOne<Picture>(p => p.Picture)
                .WithMany(pp => pp.ProductPictures)
                .HasForeignKey(p => p.PictureId);
            modelBuilder.Entity<ProductPicture>()
                .HasOne<Product>(p => p.Product)
                .WithMany(pp => pp.ProductPictures)
                .HasForeignKey(p => p.ProductId);

            modelBuilder.Entity<UserAddress>().HasKey(ua => new { ua.UserId, ua.AddressId });
            modelBuilder.Entity<UserAddress>()
                .HasOne<Address>(a => a.Address)
                .WithMany(ua => ua.UserAddresses)
                .HasForeignKey(a => a.AddressId);
            modelBuilder.Entity<UserAddress>()
                .HasOne<ApplicationUser>(u => u.User)
                .WithMany(ua => ua.UserAddresses)
                .HasForeignKey(u => u.UserId);

            modelBuilder.Entity<ProductReview>()
                .HasOne<Product>(p => p.Product)
                .WithMany(pr => pr.ProductReviews)
                .HasForeignKey(p => p.ProductId);
            modelBuilder.Entity<ProductReview>()
                .HasOne<ApplicationUser>(u => u.User)
                .WithMany(pr => pr.ProductReviews)
                .HasForeignKey(u => u.UserId);

            modelBuilder.Entity<ReturnRequest>()
                .HasOne<ApplicationUser>(u => u.User)
                .WithMany(rr => rr.ReturnRequests)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<ReturnRequest>()
                .HasOne<Store>(s => s.Store)
                .WithMany(rr => rr.ReturnRequests)
                .HasForeignKey(s => s.StoreId);
        }


        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Complain> Complains { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderItem> OrderItems { get; set; }
        public virtual DbSet<OrderNote> OrderNotes { get; set; }
        public virtual DbSet<Picture> Pictures { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        //public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<ProductPicture> ProductPictures { get; set; }
        public virtual DbSet<ProductReview> ProductReviews { get; set; }
        public virtual DbSet<ReturnRequest> ReturnRequests { get; set; }
        public virtual DbSet<ShoppingCartItem> ShoppingCartItems { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<UserAddress> UserAddresses { get; set; }
    }
}
