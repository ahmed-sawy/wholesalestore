﻿using System.Threading.Tasks;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.Repositories.GenericRepo;

namespace WholesaleStore.Persistance.UnitOfWorks
{
    public interface IUnitOfWork
    {
        IRepository<Store> StoreRepository { get; }
        IRepository<Address> AddressRepository { get; }
        IRepository<ApplicationRole> RoleRepository { get; }
        IRepository<ApplicationUser> UserRepository { get; }
        IRepository<ApplicationUserRole> UserRoleRepository { get; }
        IRepository<Category> CategoryRepository { get; }
        IRepository<Complain> ComplainRepository { get; }
        IRepository<Log> LogRepository { get; }
        IRepository<Order> OrderRepository { get; }
        IRepository<OrderItem> OrderItemRepository { get; }
        IRepository<OrderNote> OrderNoteRepository { get; }
        IRepository<Picture> PictureRepository { get; }
        IRepository<Product> ProductRepository { get; }
        //IRepository<ProductCategory> ProductCategoryRepository { get; }
        IRepository<ProductPicture> ProductPictureRepository { get; }
        IRepository<ProductReview> ProductReviewRepository { get; }
        IRepository<ReturnRequest> ReturnRequestRepository { get; }
        IRepository<ShoppingCartItem> ShoppingCartItemRepository { get; }
        IRepository<UserAddress> UserAddressRepository { get; }


        void Save();
        Task SaveAsync();
    }
}
