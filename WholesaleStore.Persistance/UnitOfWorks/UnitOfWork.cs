﻿using System;
using System.Threading.Tasks;
using WholesaleStore.Persistance.Data;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.Repositories.GenericRepo;

namespace WholesaleStore.Persistance.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _context;
        private readonly IRepository<Store> _storeRepository;
        private readonly IRepository<Address> _addressRepository;
        private readonly IRepository<ApplicationRole> _roleRepository;
        private readonly IRepository<ApplicationUser> _userRepository;
        private readonly IRepository<ApplicationUserRole> _userRoleRepository;
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<Complain> _complainRepository;
        private readonly IRepository<Log> _logRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<OrderNote> _orderNoteRepository;
        private readonly IRepository<Picture> _pictureRepository;
        private readonly IRepository<Product> _productRepository;
        //private readonly IRepository<ProductCategory> _productCategoryRepository;
        private readonly IRepository<ProductPicture> _productPictureRepository;
        private readonly IRepository<ProductReview> _productReviewRepository;
        private readonly IRepository<ReturnRequest> _returnRequestRepository;
        private readonly IRepository<ShoppingCartItem> _shoppingCartItemRepository;
        private readonly IRepository<UserAddress> _userAddressRepository;


        public UnitOfWork(
            ApplicationDbContext context,
            IRepository<Store> storeRepository,
            IRepository<Address> addressRepository,
            IRepository<ApplicationRole> roleRepository,
            IRepository<ApplicationUser> userRepository,
            IRepository<ApplicationUserRole> userRoleRepository,
            IRepository<Category> categoryRepository,
            IRepository<Complain> complainRepository,
            IRepository<Log> logRepository,
            IRepository<Order> orderRepository,
            IRepository<OrderItem> orderItemRepository,
            IRepository<OrderNote> orderNoteRepository,
            IRepository<Picture> pictureRepository,
            IRepository<Product> productRepository,
            //IRepository<ProductCategory> productCategoryRepository,
            IRepository<ProductPicture> productPictureRepository,
            IRepository<ProductReview> productReviewRepository,
            IRepository<ReturnRequest> returnRequestRepository,
            IRepository<ShoppingCartItem> shoppingCartItemRepository,
            IRepository<UserAddress> userAddressRepository
            )
        {
            _context = context;
            _storeRepository = storeRepository;
            _addressRepository = addressRepository;
            _roleRepository = roleRepository;
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
            _categoryRepository = categoryRepository;
            _complainRepository = complainRepository;
            _logRepository = logRepository;
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
            _orderNoteRepository = orderNoteRepository;
            _pictureRepository = pictureRepository;
            _productRepository = productRepository;
            //_productCategoryRepository = productCategoryRepository;
            _productPictureRepository = productPictureRepository;
            _productReviewRepository = productReviewRepository;
            _returnRequestRepository = returnRequestRepository;
            _shoppingCartItemRepository = shoppingCartItemRepository;
            _userAddressRepository = userAddressRepository;
        }


        public IRepository<Store> StoreRepository
        {
            get
            {
                return _storeRepository;
            }
        }

        public IRepository<Address> AddressRepository
        {
            get
            {
                return _addressRepository;
            }
        }

        public IRepository<ApplicationRole> RoleRepository
        {
            get
            {
                return _roleRepository;
            }
        }

        public IRepository<ApplicationUser> UserRepository
        {
            get
            {
                return _userRepository;
            }
        }

        public IRepository<ApplicationUserRole> UserRoleRepository
        {
            get
            {
                return _userRoleRepository;
            }
        }

        public IRepository<Category> CategoryRepository
        {
            get
            {
                return _categoryRepository;
            }
        }

        public IRepository<Complain> ComplainRepository
        {
            get
            {
                return _complainRepository;
            }
        }

        public IRepository<Log> LogRepository
        {
            get
            {
                return _logRepository;
            }
        }

        public IRepository<Order> OrderRepository
        {
            get
            {
                return _orderRepository;
            }
        }

        public IRepository<OrderItem> OrderItemRepository
        {
            get
            {
                return _orderItemRepository;
            }
        }

        public IRepository<OrderNote> OrderNoteRepository
        {
            get
            {
                return _orderNoteRepository;
            }
        }

        public IRepository<Picture> PictureRepository
        {
            get
            {
                return _pictureRepository;
            }
        }

        public IRepository<Product> ProductRepository
        {
            get
            {
                return _productRepository;
            }
        }

        //public IRepository<ProductCategory> ProductCategoryRepository
        //{
        //    get
        //    {
        //        return _productCategoryRepository;
        //    }
        //}

        public IRepository<ProductPicture> ProductPictureRepository
        {
            get
            {
                return _productPictureRepository;
            }
        }

        public IRepository<ProductReview> ProductReviewRepository
        {
            get
            {
                return _productReviewRepository;
            }
        }

        public IRepository<ReturnRequest> ReturnRequestRepository
        {
            get
            {
                return _returnRequestRepository;
            }
        }

        public IRepository<ShoppingCartItem> ShoppingCartItemRepository
        {
            get
            {
                return _shoppingCartItemRepository;
            }
        }

        public IRepository<UserAddress> UserAddressRepository
        {
            get
            {
                return _userAddressRepository;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            if (this._context == null)
            {
                return;
            }

            this._context.Dispose();
            this._context = null;
        }

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task SaveAsync()
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
