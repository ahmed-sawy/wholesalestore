﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WholesaleStore.Core.IServices;
using WholesaleStore.Core.Services;
using WholesaleStore.Infrastructure.CustomFilters;
using WholesaleStore.Persistance.Data;
using WholesaleStore.Persistance.Data.Entities;
using WholesaleStore.Persistance.Repositories.GenericRepo;
using WholesaleStore.Persistance.UnitOfWorks;

namespace WholesaleStore.Infrastructure.ExtensionMethods
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterRepositories(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
        }

        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IStoreService), typeof(StoreService));
            services.AddScoped(typeof(IAddressService), typeof(AddressService));
            services.AddScoped(typeof(ICategoryService), typeof(CategoryService));
            services.AddScoped(typeof(IComplainService), typeof(ComplainService)); ;
            services.AddScoped(typeof(ILogService), typeof(LogService));
            services.AddScoped(typeof(IOrderItemService), typeof(OrderItemService));
            services.AddScoped(typeof(IOrderNoteService), typeof(OrderNoteService));
            services.AddScoped(typeof(IOrderService), typeof(OrderService));
            services.AddScoped(typeof(IPictureService), typeof(PictureService));
            services.AddScoped(typeof(IProductCategoryService), typeof(ProductCategoryService));
            services.AddScoped(typeof(IProductPictureService), typeof(ProductPictureService));
            services.AddScoped(typeof(IProductReviewService), typeof(ProductReviewService));
            services.AddScoped(typeof(IProductService), typeof(ProductService));
            services.AddScoped(typeof(IReturnRequestService), typeof(ReturnRequestService));
            services.AddScoped(typeof(IRoleService), typeof(RoleService));
            services.AddScoped(typeof(IShoppingCartItemService), typeof(ShoppingCartItemService));
            services.AddScoped(typeof(IUserAddressService), typeof(UserAddressService));
            services.AddScoped(typeof(IUserRoleService), typeof(UserRoleService));
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddTransient(typeof(IEmailSender), typeof(EmailSender));
            services.AddTransient(typeof(ITokenService), typeof(TokenService));
            services.AddTransient(typeof(IAuthenticateService), typeof(AuthenticateService));
        }

        public static void RegisterCustomFilters(this IServiceCollection services)
        {
            services.AddScoped<CustomeExceptionFilter>();
        }

        public static void AddDatabaseContext(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
        }

        public static void RegisterIdentity(this IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, ApplicationRole>(options => options.SignIn.RequireConfirmedAccount = true)
                 .AddDefaultUI()
                 .AddEntityFrameworkStores<ApplicationDbContext>()
                 .AddDefaultTokenProviders();
        }

        public static void RegisterJWTAuthentication(this IServiceCollection services)
        {
            //services.AddIdentity<ApplicationUser, ApplicationRole>(options => options.SignIn.RequireConfirmedAccount = true)
            //     .AddDefaultUI()
            //     .AddEntityFrameworkStores<ApplicationDbContext>()
            //     .AddDefaultTokenProviders();
        }
    }
}
