﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WholesaleStore.Infrastructure.CustomFilters
{
    public class Roles
    {
        public const string SUPER_ADMIN = "SuperAdmin";
        public const string ADMIN = "Admin";
        public const string EMPLOYEE = "Employee";
    }

    public class Policies
    {
        public const string STORE_ADMIN = "StoreAdmin";
        public const string ADMIN = "Admin";
        public const string EMPLOYEE = "Employee";
    }
}
