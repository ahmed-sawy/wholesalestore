﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Linq;
using System.Net;
using WholesaleStore.Core.IServices;
using WholesaleStore.Models.Models;

namespace WholesaleStore.Infrastructure.CustomFilters
{
    public class AuthorizeFilter : IAuthorizationFilter
    {
        readonly string[] _claims;
        private readonly IUserService _userService;

        public AuthorizeFilter(IUserService userService, params string[] claims)
        {
            _userService = userService;
            _claims = claims;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            bool isApiRequest = context.HttpContext.Request.Path.Value.StartsWith("/api");

            var IsAuthenticated = context.HttpContext.User.Identity.IsAuthenticated;
            if (IsAuthenticated)
            {
                bool flagClaim = false;
                var user = _userService.GetByName(context.HttpContext.User.Identity.Name).Result;

                if (user != null)
                {
                    bool isSuperAdmin = user.Roles.Any(r => r.Name.ToLower() == Roles.SUPER_ADMIN.ToLower());
                    if (isSuperAdmin)
                    {
                        flagClaim = true;
                    }
                    else
                    {
                        if (_claims != null && _claims.Length > 0)
                        {
                            foreach (var claim in _claims)
                            {
                                switch (claim)
                                {
                                    case Roles.ADMIN:
                                        int storeId = isApiRequest ? int.Parse(context.RouteData.Values["storeId"].ToString()) : int.Parse(context.HttpContext.Request.Query["storeId"].ToString());
                                        flagClaim = user.Roles.Any(r => r.Name.ToLower() == Roles.ADMIN.ToLower() && user.RegisteredInStoreId == storeId);
                                        break;

                                    case Roles.SUPER_ADMIN:
                                        flagClaim = user.Roles.Any(r => r.Name.ToLower() == Roles.SUPER_ADMIN.ToLower());
                                        break;

                                    case Roles.EMPLOYEE:
                                        storeId = isApiRequest ? int.Parse(context.RouteData.Values["storeId"].ToString()) : int.Parse(context.HttpContext.Request.Query["storeId"].ToString());
                                        flagClaim = user.Roles.Any(r => r.Name.ToLower() == Roles.EMPLOYEE.ToLower() && user.RegisteredInStoreId == storeId);
                                        break;

                                    default:
                                        flagClaim = true;
                                        break;
                                }
                            }
                        }
                        else
                        {
                            flagClaim = true;
                        }
                    }
                }

                if (!flagClaim)
                {
                    if (isApiRequest)
                    {
                        var apiResponse = new { ResponseCode = (int)HttpStatusCode.Unauthorized, ResponseMessage = "Access Denied, You can't procced in this action" };
                        context.Result = new JsonResult(apiResponse);
                        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    }
                    else
                    {
                        var result = new ViewResult { ViewName = "Error" };
                        var modelMetadata = new EmptyModelMetadataProvider();
                        result.ViewData = new ViewDataDictionary(modelMetadata, context.ModelState);
                        result.ViewData.Add("ErrorMsg", "Access Denied, You can't procced in this action");
                        context.Result = result;
                        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    }
                }
            }
            else
            {
                if (isApiRequest)
                {
                    var apiResponse = new { ResponseMessage = "Please login to procceed in this action", ResponseCode = (int)HttpStatusCode.Forbidden };
                    context.Result = new JsonResult(apiResponse);
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                }
                else
                {
                    context.Result = new ViewResult() { ViewName = "/Identity/Account/Login" };
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                }
            }
        }

        //public void OnAuthorization(AuthorizationFilterContext context)
        //{
        //    var IsAuthenticated = context.HttpContext.User.Identity.IsAuthenticated;
        //    if (IsAuthenticated)
        //    {
        //        var user = _userService.GetByName(context.HttpContext.User.Identity.Name).Result;
        //        bool flagClaim = false;

        //        bool isSuperAdmin = user.Roles.Any(r => r.Name.ToLower() == Roles.SUPER_ADMIN.ToLower() && r.School == null && r.Activity == null);
        //        if (isSuperAdmin)
        //        {
        //            flagClaim = true;
        //        }
        //        else
        //        {
        //            if(_claims != null && _claims.Length > 0)
        //            {
        //                foreach (var claim in _claims)
        //                {
        //                    switch (claim)
        //                    {
        //                        case Roles.ADMIN:
        //                            flagClaim = user.Roles.Any(r => r.Name.ToLower() == Roles.ADMIN.ToLower() && r.School == null && r.Activity == null);
        //                            break;

        //                        case Roles.SUPER_ADMIN:
        //                            flagClaim = user.Roles.Any(r => r.Name.ToLower() == Roles.SUPER_ADMIN.ToLower() && r.School == null && r.Activity == null);
        //                            break;

        //                        case Roles.SCHOOL_ADMIN:
        //                            Guid schoolId = GetSchoolId(context.HttpContext.Request);
        //                            flagClaim = user.Roles.Any(r => r.Name.ToLower() == Roles.ADMIN.ToLower() && (r.School != null ? r.School.Id == schoolId : false) && r.Activity == null);
        //                            break;

        //                        case Roles.SCHOOL_SUPER_ADMIN:
        //                            schoolId = GetSchoolId(context.HttpContext.Request);
        //                            flagClaim = user.Roles.Any(r => r.Name.ToLower() == Roles.SUPER_ADMIN.ToLower() && (r.School != null ? r.School.Id == schoolId : false) && r.Activity == null);
        //                            break;

        //                        case Roles.SCHOOL_ACTIVITY:
        //                            Guid activityId = Guid.Parse(context.HttpContext.Request.Query["activityId"]);
        //                            var activity = _activityService.GetById(activityId).Result;
        //                            schoolId = GetSchoolId(context.HttpContext.Request);
        //                            flagClaim = user.Roles.Any(r => r.Name.ToLower() == activity.Name.ToLower() && (r.School != null ? r.School.Id == schoolId : false) && (r.Activity != null ? r.Activity.Id == activityId : false));
        //                            break;

        //                        default:
        //                            flagClaim = true;
        //                            break;
        //                    }
        //                    //if (context.HttpContext.User.HasClaim("ACCESS_LEVEL", claim))
        //                    //{
        //                    //    flagClaim = true;
        //                    //    break;
        //                    //}
        //                }
        //            }
        //            else
        //            {
        //                flagClaim = true;
        //            }
        //        }
        //        if (!flagClaim)
        //        {
        //            var result = new ViewResult { ViewName = "Error" };
        //            var modelMetadata = new EmptyModelMetadataProvider();
        //            result.ViewData = new ViewDataDictionary(modelMetadata, context.ModelState);
        //            result.ViewData.Add("ErrorMsg", "Access Denied, You can't procced in this action");
        //            context.Result = result;
        //            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
        //        }

        //    }
        //    else
        //    {
        //        context.Result = new ViewResult() { ViewName = "/Identity/Account/Login" };
        //        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
        //    }
        //}

        //private Guid GetSchoolId(HttpRequest request)
        //{
        //    if(!string.IsNullOrEmpty(request.Query["schoolId"]))
        //    {
        //        return Guid.Parse(request.Query["schoolId"]);
        //    }
        //    else if (!string.IsNullOrEmpty(request.Query["gradeId"]))
        //    {
        //        return (_gradesService.GetGradeSchool(request.Query["gradeId"]).Result).Id;
        //    }
        //    return Guid.Empty;
        //}
    }
}